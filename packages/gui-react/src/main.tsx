import { FC, PropsWithChildren, createContext, useContext } from "react";
import { GUIBase } from "@basthon/gui-base";

const GUIContext = createContext<GUIBase | null>(null);
const useGUI = () => useContext(GUIContext)!;
let gui: GUIBase | undefined;

interface BasthonContextProps {
  GUI?: typeof GUIBase;
  rootPath?: string;
  kernel?: string;
  pyodideURLs?: string[];
  noCheckpointsInit?: boolean;
  noInitGUI?: boolean;
}

/**
 * This (pseudo) context is in charge of basthon
 * kernel loading.
 */
const BasthonContext: FC<PropsWithChildren<BasthonContextProps>> = (props) => {
  if (props.GUI == null) throw Error("Basthon GUI class not set");
  const rootPath = props.rootPath ?? "assets";
  const pyodideURLs = props.pyodideURLs;
  const noCheckpointsInit = props.noCheckpointsInit ?? false;
  const noInitGUI = props.noInitGUI ?? false;
  let kernel = props.kernel;

  // set kernel
  if (kernel == null) {
    const url = new URL(window.location.href);
    const params = url.searchParams;
    kernel = params.get("kernel")?.toLowerCase() ?? "python3";
  }

  // set legacy
  let legacy: boolean = false;
  if (kernel.endsWith("-legacy")) {
    kernel = kernel.slice(0, -"-legacy".length);
    legacy = true;
  }

  // set language
  kernel =
    {
      // shortcuts
      py: "python3",
      python: "python3",
      "python3-old": "python3", // for backward compatibility
      js: "javascript",
    }[kernel] ?? kernel;
  const language = kernel;
  // transition phase: we force legacy mode for pyhton3 and ocaml kernels
  legacy = legacy === true || language === "python3" || language === "ocaml";

  if (gui == null) {
    gui = new props.GUI({
      kernelOptions: {
        rootPath,
        language,
        legacy,
        pyodideURLs,
      },
      noCheckpointsInit,
    });
    if (!noInitGUI) gui.init();
  }

  return (
    <GUIContext.Provider value={gui!}>{props.children}</GUIContext.Provider>
  );
};

export { BasthonContext, useGUI, type BasthonContextProps };
