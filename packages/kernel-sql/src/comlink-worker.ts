import { expose } from "@basthon/kernel-base/worker";
import { SQLKernelWorker } from "./worker";

declare let self: DedicatedWorkerGlobalScope;

expose(SQLKernelWorker);
