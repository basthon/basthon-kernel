import { expose } from "comlink";
import { SQLKernelWorker } from "./worker";

const worker = new SQLKernelWorker();

expose(worker);
