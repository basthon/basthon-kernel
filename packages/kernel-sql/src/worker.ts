import { KernelWorkerBase } from "@basthon/kernel-base/worker";
import initSqlJs, { SqlJsStatic, Database, SqlValue } from "sql.js";

type FormatedResult = { "text/plain": string; "text/html": string };

declare let self: DedicatedWorkerGlobalScope;

export class SQLKernelWorker extends KernelWorkerBase {
  private _dots: {
    [key: string]: { callback: () => unknown; help: string };
  };
  private SQL?: SqlJsStatic;
  private db?: Database;

  constructor(options?: any) {
    // do not forget to call the parent constructor
    super(options);
    // dot commands
    this._dots = {
      ".backup": {
        callback: this._export.bind(this),
        help: "Download as binary file",
      },
      ".dump": {
        callback: this._dump.bind(this),
        help: "Download as SQL file",
      },
      ".export": {
        callback: this._export.bind(this),
        help: "See .backup",
      },
      ".help": {
        callback: this._help.bind(this),
        help: "Show this help",
      },
      ".save": {
        callback: this._export.bind(this),
        help: "See .backup",
      },
      ".schema": {
        callback: this._schema.bind(this),
        help: "Show the CREATE statements",
      },
      ".tables": {
        callback: this._tables.bind(this),
        help: "List names of tables",
      },
    };
  }

  public language() {
    return "sql";
  }

  /**
   * Set the database.
   */
  protected async setDB(db?: Uint8Array): Promise<void> {
    this.db?.close?.();
    const SQL = this.SQL as SqlJsStatic;
    this.db = new SQL.Database(db);
    // foreign keys are activated by default
    // see: https://www.sqlite.org/foreignkeys.html#fk_enable
    this.runCode("PRAGMA foreign_keys = ON;");
  }

  /*
   * Initialize the kernel.
   */
  protected async _init(): Promise<void> {
    const sqlWasm = (await import("./wasm-wrap")).default;
    this.SQL = await initSqlJs({ locateFile: () => sqlWasm });
    await this.setDB();
  }

  /**
   * Run SQL code without returning anything.
   *
   * This is mainly done to bypass the db.run memory's limitation:
   * https://github.com/sql-js/sql.js/issues/482
   */
  public runCode(code: string) {
    if (this.db == null) return;
    // FIXME: this should be replaced by this.db.run(code) once
    // https://github.com/sql-js/sql.js/issues/482
    // is fixed since this should be faster than the
    // statement split used here...
    for (const statement of this.db.iterateStatements(code)) {
      while (statement.step());
    }
  }

  /**
   * Troncate json table to 150 items.
   */
  private static troncate_table(json: any) {
    const max_size = 150;
    if (json.values.length >= max_size) {
      const half_length = Math.floor(max_size / 3);
      const head = json.values.slice(0, half_length);
      const tail = json.values.slice(-half_length);
      json.values = head.concat([json.columns.map(() => "⋮ ")], tail);
    }
  }

  /**
   * HTML version of a table (useful in notebook).
   */
  private static html_table(json: any) {
    this.troncate_table(json);
    const columns = json.columns.map((c: string) => `<th>${c}</th>`).join("");
    const head = `<thead><tr style="text-align: right;">${columns}</tr></thead>`;
    const values = json.values
      .map(
        (r: string[]) =>
          "<tr>" + r.map((v) => `<td>${v}</td>`).join("") + "</tr>",
      )
      .join("");
    const body = `<tbody>${values}</tbody>`;
    return `<table class="dataframe" border="1">${head}${body}</table>`;
  }

  /**
   * Text version of a table.
   */
  private static text_table(json: any) {
    SQLKernelWorker.troncate_table(json);
    const head = json.columns.join("\t");
    const values = json.values.map((r: string[]) => r.join("\t")).join("\n");
    return `${head}\n${values}`;
  }

  /**
   * Format the result to fit Kernel API.
   */
  private formatResult(execResult: any): FormatedResult | undefined {
    if (execResult == null) return undefined;
    return {
      "text/plain": SQLKernelWorker.text_table(execResult),
      "text/html": SQLKernelWorker.html_table(execResult),
    };
  }

  /**
   * Execute SQL code and return an object containing columns names
   * (.columns) and values (.values).
   */
  private execCode(code: string, format: boolean = true) {
    if (this.db == null) return;
    let columns: string[] | undefined;
    let values: SqlValue[][] | undefined;
    // see:
    // https://github.com/sql-js/sql.js/pull/388#issuecomment-712405389
    for (const statement of this.db.iterateStatements(code)) {
      columns = statement.getColumnNames();
      values = [];
      while (statement.step()) values.push(statement.get());
    }
    // only last statement is relevent
    if ((columns?.length ?? 0) === 0) return undefined;
    const res = { columns, values };
    if (format) return this.formatResult(res);
    return res;
  }

  protected async _eval(data: any, code: string): Promise<any> {
    const dotCommand = code.replace(/^[; \t\n]+|[; \t\n]+$/g, "");

    if (dotCommand in this._dots) {
      const callback = this._dots[dotCommand].callback as () => unknown;
      return callback();
    } else return this.execCode(code);
  }

  /**
   * Special case of starting a legacy kernel.
   */
  public async legacyStart(): Promise<void> {
    await this.setDB();
  }

  /**
   * Special case of stoping a legacy kernel.
   */
  public async legacyStop(): Promise<void> {
    this.db?.close?.();
    this.db = undefined;
  }

  public async more(code: string): Promise<boolean> {
    return false;
  }

  public async putFile(filename: string, content: ArrayBuffer): Promise<void> {
    // just ignoring this since we don't have a filesystem.
  }

  public async putModule(filename: string, content: ArrayBuffer) {
    const _content = new Uint8Array(content);
    const ext = filename.split(".").pop();
    switch (ext) {
      case "sql":
        let decoder = new TextDecoder("utf-8");
        const __content = decoder.decode(_content);
        this.runCode(__content);
        break;
      case "sqlite":
      case "db":
        await this.setDB(_content);
        break;
      default:
        throw { message: "Only '.sql', '.db' and '.sqlite' files supported." };
    }
  }

  /**
   * Display help.
   */
  private _help() {
    const helpMessage = Object.keys(this._dots)
      .map((key: string) => `${key}\t\t${this._dots[key].help}`)
      .join("\n");
    return { "text/plain": helpMessage };
  }

  /**
   * Export (download) the database in binary format.
   */
  private _export(): void {
    if (this.db == null) return;
    this.download(this.db.export(), "database.db");
  }

  /**
   * Return the SQL commands to build the DB in a string.
   */
  private dump(): string {
    if (this.db == null) return "";
    // order is creation order (order=false)
    const tables = this.tables(false)?.values ?? [];
    const schema = this.schema()?.values ?? [];
    const inserts = tables.map((table: string) => {
      const values = this.db?.exec(`SELECT * FROM ${table}`)[0]?.values ?? [];
      const strValues = values.map(
        (value: any) => `(${value.map(JSON.stringify).join(", ")})`,
      );
      return `INSERT INTO ${table} VALUES ${strValues.join(",\n")};`;
    });
    return `${schema.join(";\n")};\n${inserts.join("\n")}`;
  }

  /**
   * Dump (download) the database in SQL format (dot command version).
   */
  private _dump(): void {
    const encoder = new TextEncoder();
    const view = encoder.encode(this.dump());
    this.download(view, "database.sql");
  }

  /**
   * Return the DB schema.
   */
  private schema(order = false) {
    const orderCMD = order ? "ORDER BY tbl_name, type DESC, name" : "";
    return this.execCode(
      `SELECT sql FROM sqlite_schema WHERE type="table" ${orderCMD};`,
      false,
    ) as { [key: string]: string[] } | undefined;
  }

  /**
   * Return the DB schema (dot command version).
   */
  private _schema(): FormatedResult | undefined {
    return this.formatResult(this.schema());
  }

  /**
   * Return the table of tables.
   */
  private tables(order = true) {
    const orderCMD = order ? "ORDER BY 1" : "";
    return this.execCode(
      `SELECT name FROM sqlite_schema
WHERE type IN ('table','view') AND name NOT LIKE 'sqlite_%'
${orderCMD};`,
      false,
    ) as { [key: string]: string[] } | undefined;
  }

  /**
   * Return the table of tables (dot command version).
   */
  private _tables(): FormatedResult | undefined {
    return this.formatResult(this.tables());
  }
}
