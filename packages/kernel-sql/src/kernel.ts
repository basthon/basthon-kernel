import { KernelMainBase } from "@basthon/kernel-base/worker";
import { SQLKernelWorker } from "./worker";

export class SQLKernel extends KernelMainBase<SQLKernelWorker> {
  public constructor(options: any) {
    super(options);
  }

  protected newWorker(): Worker {
    return new Worker(new URL("./comlink-worker.js", import.meta.url), {
      //@ts-ignore
      sandboxed: true, // removing this line will cause security issues
    });
  }

  protected async importLegacyWorker(): Promise<void> {
    await import("./comlink-worker.js");
  }

  language() {
    return "sql";
  }

  languageName() {
    return "SQL";
  }

  moduleExts() {
    return ["sql", "db", "sqlite"];
  }

  public ps1() {
    return "sql> ";
  }

  public ps2() {
    return "...> ";
  }
}
