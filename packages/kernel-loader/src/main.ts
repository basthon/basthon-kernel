import { PromiseDelegate } from "promise-delegate";
import { KernelBase } from "@basthon/kernel-base";

declare global {
  interface Window {
    Basthon?: KernelBase;
  }
}

/**
 * Helper class to dynamically load kernels.
 */
export class KernelLoader {
  private _kernel?: KernelBase;
  private readonly _pageLoad = new PromiseDelegate<void>();
  private readonly _kernelAvailable = new PromiseDelegate<KernelBase>();
  private _loaderId = "basthon-loader";
  /**
   * Flag to force loader to be shown.
   * Basically, loader can be hidden after Basthon loading.
   */
  private _doNotHideLoader = true;
  private _rootLoader?: HTMLDivElement;
  private _loaderTextElem?: HTMLDivElement;
  private _loaderTextError?: HTMLDivElement;

  constructor(options: any, loaderId?: string) {
    const language: string = options.language;
    if (loaderId != null) this._loaderId = loaderId;

    if (document.readyState === "loading") {
      const callback = () => {
        document.removeEventListener("DOMContentLoaded", callback);
        this._pageLoad.resolve();
      };
      document.addEventListener("DOMContentLoaded", callback);
    } else {
      // `DOMContentLoaded` has already fired
      this._pageLoad.resolve();
    }

    // Dynamicaly import to allow webpack chuncks
    (async () => {
      switch (language) {
        case "echo":
          const { EchoKernel } = await import("@basthon/kernel-echo");
          this._kernel = new EchoKernel(options);
          break;
        case "python":
        case "python3":
        case "python3.11":
        case "python3-old": // keep this for backward compatibility
        case "python3.8":
          const { Python3Kernel } = await import("@basthon/kernel-python3");
          this._kernel = new Python3Kernel(options);
          break;
        case "js":
        case "javascript":
          const { JavaScriptKernel } = await import(
            "@basthon/kernel-javascript"
          );
          this._kernel = new JavaScriptKernel(options);
          break;
        case "sql":
          const { SQLKernel } = await import("@basthon/kernel-sql");
          this._kernel = new SQLKernel(options);
          break;
        case "ocaml":
          const { OCamlKernel } = await import("@basthon/kernel-ocaml");
          this._kernel = new OCamlKernel(options);
          break;
        case "xcas":
          const { XCASKernel } = await import("@basthon/kernel-xcas");
          this._kernel = new XCASKernel(options);
          break;
        default:
          window.console.error(`Kernel '${language}' not supported.`);
          this._kernelAvailable.reject();
          return;
      }
      // Some kernels need this e.g. Python3Kernel).
      window.Basthon = this._kernel;
      this._kernelAvailable.resolve(this._kernel);
    })();
  }

  /**
   * Returns a promise that resolves when page is loaded
   * (document.body available).
   */
  public async pageLoad() {
    await this._pageLoad.promise;
  }

  /**
   * Is the kernel object available (not null)?
   * Be careful, it does not resolves when the kernel is started but when
   * it is set. See kernelStarted.
   */
  public async kernelAvailable(): Promise<KernelBase> {
    return await this._kernelAvailable.promise;
  }

  /**
   * Is the kernel started?
   */
  public async kernelStarted(): Promise<KernelBase> {
    const kernel = await this.kernelAvailable();
    await kernel.ready();
    return kernel;
  }

  /**
   * Kernel getter.
   */
  public get kernel() {
    return this._kernel;
  }

  /**
   * Access the kernel or null if not ready
   */
  public get kernelSafe() {
    return this.kernel?.isReady ? this.kernel : null;
  }

  /**
   * Show a fullscreen loader that disapear when Basthon is loaded.
   * If you want to manually hide the loader, set hideAfter to false.
   */
  public async showLoader(
    text: string,
    fullscreen: boolean = false,
    hideAfter: boolean = true,
  ) {
    // kernel init
    (async (): Promise<void> => {
      const kernel = await this.kernelAvailable();
      // no need to let this buble up since it is catched
      // later by this kernel loader
      try {
        await kernel.init();
      } catch (e) {}
    })();

    // dynamically import css
    await import("./style.css");

    const layer = `basthon-loader-${fullscreen ? "full" : "foot"}`;
    // dynamically adding the loader to the DOM
    const root = document.createElement("div");
    root.id = this._loaderId;
    root.classList.add("darklighted");
    root.classList.add("basthon-loader-root");
    root.classList.add(layer);
    const container = document.createElement("div");
    container.classList.add("basthon-loader-container");
    container.classList.add(layer);
    root.appendChild(container);
    const loader = document.createElement("div");
    loader.classList.add("basthon-loader-spinner");
    loader.classList.add(layer);
    loader.innerHTML = "<span><span></span><span></span></span>";
    container.appendChild(loader);
    const space = document.createElement("div");
    space.classList.add("basthon-loader-break");
    space.classList.add(layer);
    container.appendChild(space);
    const textElem = document.createElement("div");
    textElem.classList.add("basthon-loader-text");
    textElem.classList.add(layer);
    textElem.innerHTML = text;
    container.appendChild(textElem);
    this._loaderTextElem = textElem;
    const textError = document.createElement("div");
    textError.classList.add("basthon-loader-text");
    textError.classList.add(layer);
    textError.style.display = "none";
    container.appendChild(textError);
    this._loaderTextError = textError;

    await this.pageLoad();
    document.body.appendChild(root);

    // backup root
    this._rootLoader = root;

    // waiting for Basthon
    try {
      await this.kernelStarted();
    } catch (e) {
      const browser = this.browser();
      this.setErrorText(
        "Erreur de chargement de Basthon !!!" +
          "<br>" +
          "Vérifiez que votre navigateur est à jour." +
          "<br>" +
          `Version détectée : ${browser.name} ${browser.version}.`,
      );
      for (const elem of document.body.querySelectorAll(
        ".basthon-loader-foot",
      )) {
        elem.classList.remove("basthon-loader-foot");
        elem.classList.add("basthon-loader-full");
      }
      loader.classList.remove("basthon-loader-spinner");
      loader.classList.add("basthon-loader-error");
      this._doNotHideLoader = true;
      return;
    }

    this._doNotHideLoader = false;

    // hide the loader if requested
    if (hideAfter) {
      this.hideLoader();
    }
  }

  /**
   * Setting the text loader.
   */
  public setLoaderText(text: string) {
    if (this._loaderTextElem != null) this._loaderTextElem.innerHTML = text;
  }

  /**
   * Setting the error text.
   */
  public setErrorText(text: string) {
    if (this._loaderTextElem != null)
      this._loaderTextElem.style.display = "none";
    if (this._loaderTextError != null) {
      this._loaderTextError.style.display = "";
      this._loaderTextError.innerHTML = text;
    }
  }

  /**
   * Hide the Basthon's loader.
   */
  public hideLoader() {
    if (this._doNotHideLoader) return;
    const root = this._rootLoader;
    if (root == null) return;
    root.classList.add("basthon-loader-hide");
    window.setTimeout(() => {
      this._rootLoader = undefined;
      root.remove();
    }, 1100);
  }

  /**
   * Get browser info (name and version).
   */
  public browser() {
    var ua = navigator.userAgent;
    var tem: RegExpMatchArray | null;
    var M =
      ua.match(
        /(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i,
      ) || [];
    if (/trident/i.test(M[1])) {
      tem = /\brv[ :]+(\d+)/g.exec(ua) || ([""] as RegExpMatchArray);
      return {
        name: "IE",
        version: tem[1] || "",
      };
    }
    if (M[1] === "Chrome") {
      tem = ua.match(/\bOPR|Edge\/(\d+)/);
      if (tem != null) {
        return {
          name: "Opera",
          version: tem[1],
        };
      }
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, "-?"];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) {
      M.splice(1, 1, tem[1]);
    }
    return {
      name: M[0],
      version: M[1],
    };
  }
}
