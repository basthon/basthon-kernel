import { KernelMainBase } from "@basthon/kernel-base/worker";
import { OCamlKernelWorker } from "./worker";

/**
 * An OCaml kernel that satisfies Basthon's API.
 */
export class OCamlKernel extends KernelMainBase<OCamlKernelWorker> {
  constructor(options: any) {
    super(options);
  }

  protected newWorker(): Worker {
    return new Worker(new URL("./comlink-worker.js", import.meta.url), {
      //@ts-ignore
      sandboxed: true, // removing this line will cause security issues
    });
  }

  protected async importLegacyWorker(): Promise<void> {
    await import("./comlink-worker.js");
  }

  public language() {
    return "ocaml";
  }

  public languageName() {
    return "OCaml";
  }

  public moduleExts() {
    return ["ml"];
  }

  /**
   * List modules launched via putModule.
   */
  public async userModules(): Promise<string[]> {
    return [];
  }

  /**
   * Download a file from the VFS.
   */
  public async getFile(path: string): Promise<Uint8Array> {
    return new Uint8Array([]);
  }

  /**
   * Download a user module file.
   */
  public async getUserModuleFile(filename: string): Promise<Uint8Array> {
    return new Uint8Array([]);
  }

  /**
   * Mimic the OCaml's REPL banner.
   */
  public banner() {
    return "        OCaml version 4.11.1\n";
  }

  public ps1(): string {
    return "# ";
  }

  public ps2(): string {
    return "  ";
  }
}
