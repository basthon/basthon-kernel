import { expose } from "@basthon/kernel-base/worker";
import { OCamlKernelWorker } from "./worker";

declare let self: DedicatedWorkerGlobalScope;

expose(OCamlKernelWorker);
