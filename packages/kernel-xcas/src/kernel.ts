import { KernelMainBase } from "@basthon/kernel-base/worker";
import { XCASKernelWorker } from "./worker";

export class XCASKernel extends KernelMainBase<XCASKernelWorker> {
  public constructor(options: any) {
    super(options);
  }

  protected newWorker(): Worker {
    return new Worker(new URL("./comlink-worker.js", import.meta.url), {
      //@ts-ignore
      sandboxed: true, // removing this line will cause security issues
    });
  }

  protected async importLegacyWorker(): Promise<void> {
    await import("./comlink-worker.js");
  }

  language() {
    return "xcas";
  }

  languageName() {
    return "XCAS";
  }

  moduleExts() {
    return [];
  }

  public ps1() {
    return "xcas> ";
  }

  public ps2() {
    return "...> ";
  }
}
