import { KernelWorkerBase } from "@basthon/kernel-base/worker";
import { XCASWrapper } from "./xcas-wrap";

type FormatedResult = { "text/latex": string };

declare let self: DedicatedWorkerGlobalScope;

export class XCASKernelWorker extends KernelWorkerBase {
  private _xcas: XCASWrapper;

  constructor(options?: any) {
    // do not forget to call the parent constructor
    super(options);
    this._xcas = new XCASWrapper();
  }

  public language() {
    return "xcas";
  }

  /*
   * Initialize the kernel.
   */
  protected async _init(): Promise<void> {
    window.alert = (msg: string): void => console.error(msg);
    console.info = (...args: any[]): void => console.log(...args);
    console.warn = (...args: any[]): void => console.error(...args);
    globalThis.addEventListener("error", (e: Event): void =>
      console.error(e.toString()),
    );

    await this._xcas.init();
  }

  protected async _eval(
    data: any,
    code: string,
  ): Promise<FormatedResult | undefined> {
    console.log = (...args: any[]): void => {
      this.sendStdoutStream(data, args.join(" ") + "\n");
    };
    console.error = (...args: any[]): void => {
      this.sendStderrStream(data, args.join(" ") + "\n");
    };

    const res = this._xcas.caseval(code);
    if (res == null) return undefined;
    if (res.error != null) throw res.error;

    // use json to remove keys of undefined value
    return JSON.parse(
      JSON.stringify({
        "text/plain": res.text,
        "text/latex": res.latex,
        "image/svg+xml": res.svg,
      }),
    );
  }

  /**
   * Special case of starting a legacy kernel.
   */
  public async legacyStart(): Promise<void> {}

  /**
   * Special case of stoping a legacy kernel.
   */
  public async legacyStop(): Promise<void> {}

  public async more(code: string): Promise<boolean> {
    return false;
  }

  public async putFile(filename: string, content: ArrayBuffer): Promise<void> {
    // just ignoring this since we don't have a filesystem.
  }

  public async putModule(filename: string, content: ArrayBuffer) {}
}
