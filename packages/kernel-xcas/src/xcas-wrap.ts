declare let self: DedicatedWorkerGlobalScope;

export class XCASWrapper {
  private _giac?: any;
  private _caseval?: (t: string) => string;

  constructor() {}

  async init(): Promise<void> {
    //@ts-ignore
    const giac = (await import("@basthon/xcas-wasm")).default;
    await new Promise<void>((resolve, reject) => {
      giac.onRuntimeInitialized = resolve;
      giac.onAbort = reject;
      giac.run();
    });
    this._giac = giac;
    // get eval function from c code
    const caseval = this._giac.cwrap("caseval", "string", ["string"]);
    this._caseval = (t: string): string => caseval(t.replace(/%22/g, '"'));
  }

  unquote(text: string): string {
    return text.replace(/^"/, "").replace(/"$/, "");
  }

  latex(text: string): string {
    let res = this._caseval!(`latex(quote(${text}))`);
    res = this.unquote(res);
    return `\\[${res}\\]`;
  }

  caseval(code: string):
    | {
        text?: string;
        svg?: string;
        latex?: string;
        error?: string;
      }
    | undefined {
    // eval
    const out = this._caseval!(code).replace(/;$/, "");
    if (typeof out !== "string") return;

    // formatting
    const result: any = {};
    if (/^"<svg .*<\/svg>"$/s.test(out)) result.svg = this.unquote(out);
    else if (out.startsWith('"')) result.text = `text ${out}`;
    else if (out.startsWith("GIAC_ERROR: "))
      result.error = out.replace(/^GIAC_ERROR: /, "");
    else result.latex = this.latex((result.text = out));
    return result;
  }
}
