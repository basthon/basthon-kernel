import { expose } from "@basthon/kernel-base/worker";
import { XCASKernelWorker } from "./worker";

declare let self: DedicatedWorkerGlobalScope;

expose(XCASKernelWorker);
