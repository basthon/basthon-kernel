#!/usr/bin/env python3

from pathlib import Path
import subprocess
import shutil

# parallel calls
processes = [
    subprocess.Popen(["make", "build"], cwd=path)
    for path in Path("./src/modules").glob("*/")
]

# wait all
for p in processes:
    p.wait()
    if p.returncode != 0:
        raise RuntimeError()

# copy exter stuff
for path in Path("./src/modules").glob("*/"):
    for asset in path.glob("extern/*"):
        dst = Path("./lib/dist/modules/extern") / path.name
        dst.mkdir(parents=True, exist_ok=True)
        shutil.copy(asset, dst)

# copy wheels
for wheel in Path("./src/modules").glob("*/dist/*.whl"):
    dst = Path("./lib/dist/modules/")
    dst.mkdir(parents=True, exist_ok=True)
    shutil.copy(wheel, dst)
