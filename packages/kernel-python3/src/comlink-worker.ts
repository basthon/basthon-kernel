import { expose } from "@basthon/kernel-base/worker";
import { Python3KernelWorker } from "./worker";

declare let self: DedicatedWorkerGlobalScope;

expose(Python3KernelWorker);
