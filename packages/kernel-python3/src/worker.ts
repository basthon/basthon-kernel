import { KernelWorkerBase, loadScript } from "@basthon/kernel-base/worker";

const PYODIDE_VERSION = "0.23.2";

declare let self: DedicatedWorkerGlobalScope;

declare global {
  interface DedicatedWorkerGlobalScope {
    loadPyodide?: any;
    pyodide?: any;
  }
}

export class Python3KernelWorker extends KernelWorkerBase {
  /**
   * Where to find pyodide.js (private).
   */
  private _pyodideURLs = [
    `https://cdn.jsdelivr.net/pyodide/v{PYODIDE_VERSION}/full/pyodide.js`,
  ];
  private __kernel__: any = null;
  public pythonVersion: string = "";

  constructor(options?: any) {
    // do not forget to call the parent constructor
    super(options);

    // for locally installed Pyodide
    this._pyodideURLs.unshift(`${this.basthonRoot()}/pyodide/pyodide.js`);
    this._pyodideURLs = options?.pyodideURLs ?? this._pyodideURLs;
  }

  public language() {
    return "python3";
  }

  /**
   * Get the URL of Basthon modules dir.
   */
  public basthonModulesRoot() {
    return this.basthonRoot() + "/modules";
  }

  /**
   * What to do when loaded (private).
   */
  async _onload() {
    const pyodide = self.pyodide;
    // reformat repodata
    const packages = pyodide._api.repodata_packages;
    for (let p of Object.keys(packages)) {
      const item = packages[p];
      item.file_name = item.file_name.replace(
        "{basthonRoot}",
        this.basthonRoot(),
      );
    }
    // get the version of Python from Python
    this.pythonVersion = pyodide.runPython(
      "import platform ; platform.python_version()",
    );
    // load basthon and get kernel
    await pyodide.loadPackage("basthon");
    this.__kernel__ = pyodide.pyimport("basthon").__kernel__;

    this.__kernel__.start();
  }

  /*
   * Initialize the kernel.
   */
  protected async _init(): Promise<void> {
    let pyodideURL: string = this._pyodideURLs[0];
    for (let url of this._pyodideURLs) {
      url = url.replace("{PYODIDE_VERSION}", PYODIDE_VERSION);
      try {
        const response = await fetch(url, { method: "HEAD" });
        if (response.ok) {
          pyodideURL = url;
          break;
        }
      } catch (e) {}
    }

    try {
      await loadScript(pyodideURL);
    } catch (error) {
      console.log(error);
      console.error("Can't load pyodide.js");
      throw error;
    }

    if (self.loadPyodide == null) {
      console.log("self.loadPyodide is null!");
      throw new Error("Can't load pyodide.js");
    }

    try {
      // loading with custom repodata.json
      self.pyodide = await self.loadPyodide({
        lockFileURL: this.basthonRoot() + "/repodata.json",
      });
    } catch (error) {
      console.log(error);
      console.error("Can't load Python from Pyodide");
      throw error;
    }
    await this._onload();
  }

  /**
   * Execution count getter overload.
   */
  public get execution_count() {
    return this.__kernel__.execution_count();
  }

  /**
   * Basthon async code evaluation function.
   */
  protected async _eval(data: any, code: string): Promise<any> {
    const outCallback = (msg: string) => this.sendStdoutStream(data, msg);
    const errCallback = (msg: string) => this.sendStderrStream(data, msg);

    // dependencies are loaded by eval
    const proxy = await this.__kernel__.eval(
      code,
      outCallback,
      errCallback,
      data,
    );
    // when an error occures, proxy should be the error message
    if (!(proxy instanceof self.pyodide.ffi.PyProxy)) throw proxy;
    const res = proxy.toJs({
      create_proxies: false,
      dict_converter: Object.fromEntries,
    });
    proxy.destroy();
    return res[0];
  }

  /**
   * Special case of starting a legacy kernel.
   */
  public async legacyStart(): Promise<void> {
    this.__kernel__.start();
  }

  /**
   * Special case of stoping a legacy kernel.
   */
  public async legacyStop(): Promise<void> {
    this.__kernel__.stop();
  }

  /**
   * Complete a code at the end (usefull for tab completion).
   *
   * Returns an array of two elements: the list of completions
   * and the start index.
   */
  public async complete(code: string): Promise<[string[], number] | []> {
    const proxy = this.__kernel__.complete(code);
    if (!(proxy instanceof self.pyodide.ffi.PyProxy)) return proxy;
    const res = proxy.toJs();
    proxy.destroy();
    return res;
  }

  /**
   * Is the source ready to be evaluated or want we more?
   * Usefull to set ps1/ps2 for teminal prompt.
   */
  public async more(source: string): Promise<boolean> {
    return this.__kernel__.more(source);
  }

  /**
   * List content of directory (Python's virtual FS).
   */
  public readdir(path: string): string[] {
    return self.pyodide._module.FS.readdir(path);
  }

  /**
   * Change current directory (Python's virtual FS).
   */
  public chdir(path: string) {
    self.pyodide._module.FS.chdir(path);
  }

  /**
   * Create directory (Python's virtual FS).
   */
  public mkdir(path: string) {
    self.pyodide._module.FS.mkdir(path);
  }

  /**
   * Put a file on the local (emulated) filesystem.
   */
  public async putFile(filename: string, content: ArrayBuffer): Promise<void> {
    this.__kernel__.put_file(filename, content);
  }

  /**
   * Get a file content from the VFS.
   */
  public async getFile(path: string): Promise<Uint8Array> {
    return this.__kernel__.get_file(path).toJs();
  }

  /**
   * Put an importable module on the local (emulated) filesystem
   * and load dependencies.
   */
  public async putModule(
    filename: string,
    content: ArrayBuffer,
  ): Promise<void> {
    return await this.__kernel__.put_module(filename, content);
  }

  /**
   * Get a user module file content.
   */
  public async getUserModuleFile(filename: string): Promise<Uint8Array> {
    return this.__kernel__.get_user_module_file(filename).toJs();
  }

  /**
   * List modules launched via putModule.
   */
  public async userModules(): Promise<string[]> {
    const proxy = this.__kernel__.user_modules();
    if (!(proxy instanceof self.pyodide.ffi.PyProxy)) return proxy;
    const res = proxy.toJs();
    proxy.destroy();
    return res;
  }

  /**
   * Display a matplotlib kernel iframe from its html code.
   */
  public async displayMatplotlibIFrame(iframeHTML: string): Promise<void> {
    const port = this.displayKernelIFrame(iframeHTML);
    port.onmessage = (event) => {
      console.log(
        "Kernel Worker says: 'Message received from iframe: " +
          event.data +
          "'",
      );
      port.postMessage("hello from kernel worker");
    };
  }
}
