import builtins
import js
from . import kernel


__author__ = "Romain Casati"
__license__ = "GNU GPL v3"
__email__ = "romain.casati@basthon.fr"


__all__ = ["patch_all"]


def patch_input():
    """Patching Pyodide input function."""
    _default_input = builtins.input

    def _patched_input(prompt=None, password=False):
        return js.basthon.input(prompt, password)

    # copying all writable attributes (useful to keep docstring and name)
    for a in dir(_default_input):
        try:
            setattr(_patched_input, a, getattr(_default_input, a))
        except Exception:
            pass

    # replacing
    builtins.input = _patched_input

    # no need to patch if sync communication is supported
    if js.basthon.syncCommSupport():
        return

    # define async input
    async def input_async(*args, **kwargs):
        local_input = kernel.locals().get("input", builtins.input)
        if local_input != _patched_input:
            return local_input(*args, **kwargs)
        return await kernel.input_async(*args, **kwargs)

    builtins._basthon_input_async = input_async


def patch_sleep():
    """
    Patching time.sleep function for toplevel async calls.

    It is not a builtin function but the patch put stuff in builtins.
    """
    # no need to patch if sync communication is supported
    if js.basthon.syncCommSupport():
        return

    async def sleep_async(*args, **kwargs):
        import time

        local_sleep = kernel.locals().get("sleep", time.sleep)
        if local_sleep != time.sleep:
            return local_sleep(*args, **kwargs)
        return await kernel.sleep_async(*args, **kwargs)

    builtins._basthon_sleep_async = sleep_async


def patch_all():
    """
    Patch all builtins by calling all patch_* methods.
    """
    for name, method in globals().items():
        if name.startswith("patch_") and name != "patch_all":
            method()
