import copy
import inspect
import sys
import __main__
import traceback
from typing import Iterable, Optional, Callable, Union, Any
from .validate import validationclass
from .sequences import ValidateSequences, V, T
from .. import display

__author__ = "Romain Casati"
__license__ = "GNU GPL v3"
__email__ = "romain.casati@basthon.fr"

__all__ = [
    "ValidateFunction",
    "ValidateFunctionPretty",
]


@validationclass
class ValidateFunction(ValidateSequences[V, T]):
    """A class to validate a function. The tested function could had
    any number of parameters.

    To customize this class, have a look at :class:`ValidateSequences`.
    """

    def __init__(
        self,
        func_name: str,
        test_values: Iterable[V],
        target_values: Optional[Iterable[T]] = None,
        valid_function: Optional[Callable[..., T]] = None,
        argcount: Optional[int] = None,
        check_signature: Union[bool, inspect.Signature] = False,
        ignore_names_in_signature: bool = False,
        **kwargs,
    ):
        """
        :param func_name: the name of the function to test in the
            user namespace (top-level)
        :param test_values: values to test the function on ; for function
            with several parameters, use list or tuple
        :param return_values: target values that can be computed from
            ``valid_function`` if ``return_values`` is omitted.
        :param valid_function: a function to compute target values if
            ``target_values`` is ``None``.
        :param argcount: the number of arguments given to the function ;
            if ``None``, it is guessed from the test values
        :param check_signature: indicate if we have to check the signature
            in the validation process ; the signature is taken from
            ``valid_function`` or directly passed through
            ``check_signature``
        :param ignore_names_in_signature: indicate if we have to worry about
            wrong parameter name in the function's signature
        :param \**kwargs: passed to parent constructor.
        """
        # copy test values in case it is just an iterator
        test_values = list(test_values)
        # guess argcount
        if argcount is None:
            argcounts = set(
                len(v) if isinstance(v, (tuple, list)) else 1 for v in test_values
            )
            argcount = argcounts.pop() if len(argcounts) == 1 else 1
        # build target values from valid function
        if valid_function is not None:
            target_values = []
            for x in test_values:
                if argcount == 1:
                    x = (x,)
                # we deeply copy input in case valid_function modifies it
                x = copy.deepcopy(x)
                target_values.append(valid_function(*x))
        if target_values is None:
            raise ValueError(
                "Arguments `target_values` and `valid_function` should not be None simultaneously"
            )
        super().__init__(test_values, target_values, **kwargs)
        self._func_name: str = func_name
        self._argcount: int = argcount
        self._check_signature: bool = check_signature is not False
        self._ignore_names_in_signature: bool = ignore_names_in_signature
        self._signature: Union[inspect.Signature, None] = None
        if self._check_signature:
            if isinstance(check_signature, inspect.Signature):
                self._signature = check_signature
            elif valid_function is not None:
                self._signature = inspect.signature(valid_function)
            else:
                raise ValueError(
                    "request for signature check but no signature submited!"
                )

    def check_signature(self, func: Callable[..., T], verbose=False) -> bool:
        """Check the function signature."""
        if not self._check_signature:
            return True
        res = True
        user_sig = inspect.signature(func)
        # check number of params
        if len(user_sig.parameters) != len(self._signature.parameters):
            res = False
            if verbose:
                print(
                    f"Ta fonction '{self._func_name}' n'a pas le bon nombre de paramètres !",
                    file=sys.stderr,
                )
        else:
            # check each param separately
            for p1, p2 in zip(
                user_sig.parameters.values(), self._signature.parameters.values()
            ):
                if not self._ignore_names_in_signature and p1.name != p2.name:
                    res = False
                    if verbose:
                        print(
                            f"Dans ta fonction '{self._func_name}', le paramètre '{p1.name}' n'a pas le bon nom !",
                            file=sys.stderr,
                        )
                if p1.annotation != p2.annotation:
                    res = False
                    if verbose:
                        print(
                            f"Dans ta fonction '{self._func_name}', le paramètre '{p1.name}' n'a pas le bon type !",
                            file=sys.stderr,
                        )
        if user_sig.return_annotation != self._signature.return_annotation:
            res = False
            if verbose:
                print(
                    f"Ta fonction '{self._func_name}' n'a pas le bon type de retour !",
                    file=sys.stderr,
                )
        return res

    def precompute(self) -> Callable[..., T]:
        func = getattr(__main__, self._func_name)
        if not self.check_signature(func):
            error = TypeError("bad signature")
            error.func = func
            raise error
        return func

    def handle_undefined(self) -> None:
        """Handle the case of a function not found in the toplevel namespace."""
        print(
            f"La fonction '{self._func_name}' n'est pas définie, as-tu validé la cellule ?",
            file=sys.stderr,
        )

    def handle_bad_signature(self, func: Callable[..., T]) -> None:
        """Handle error occurring due to bad function signature."""
        self.check_signature(func, verbose=True)

    def handle_precompute_exception(self, exc: Exception) -> None:
        if isinstance(exc, AttributeError):
            self.handle_undefined()
        elif isinstance(exc, TypeError):
            self.handle_bad_signature(exc.func)
        else:
            raise exc

    def compute_result(self, value: V, precomputed_data: Any) -> T:
        func = precomputed_data
        # we deeply copy the value in case user modifies it
        value = copy.deepcopy(value)
        if self._argcount == 1:
            return func(value)
        else:
            return func(*value)

    def handle_failure(self, value: V, target: T, result: T) -> bool:
        print(
            f"Vérifie ta fonction '{self._func_name}', au moins un test n'est pas passé...",
            file=sys.stderr,
        )
        return False

    def handle_exception(self, exc: Exception, value: V, target: T) -> bool:
        traceback.print_exception(exc, limit=-1)
        return False

    def handle_full_success(self):
        print(f"👏 Bravo, ta fonction '{self._func_name}' passe tous les tests !")


@validationclass
class ValidateFunctionPretty(ValidateFunction[V, T]):
    """Similar to :class:`ValidateFunction` but with all
    test results sumurized in a pretty html table.

    To customize this class, have a look at :class:`ValidateSequences`.
    """

    def precompute(self) -> Any:
        self._test_results: list[tuple[V, T, Union[T, str], Union[bool, str]]] = []
        return super().precompute()

    def display_table(self):
        """Display the HTML result table."""
        bgclass = {True: "good", False: "bad", "error": "bad"}
        icon = {True: "✅", False: "❌", "error": "💥"}
        template = """<tr class="{bgclass}">
                        <td>{fname}({value})</td>
                        <td>{target}</td>
                        <td><b>{result}</b></td>
                        <td>{icon}</td>
                      </tr>"""
        rows = "\n".join(
            template.format(
                fname=self._func_name,
                value=(
                    repr(value)
                    if self._argcount == 1
                    else ", ".join(repr(x) for x in value)
                ),
                target=target,
                result=result,
                bgclass=bgclass[validation],
                icon=icon[validation],
            )
            for (value, target, result, validation) in self._test_results
        )
        html = f"""<table>
                     <thead>
                       <tr>
                         <th>Test</th>
                         <th>Attendu</th>
                         <th>Calculé</th>
                         <th></th>
                       </tr>
                     </thead>
                     <tbody>{rows}</tbody>
                   </table>"""
        display({"text/html": html}, raw=True)

    def handle_exception(self, exc: Exception, value: V, target: T) -> bool:
        super().handle_exception(exc, value, target)
        self._test_results.append((value, target, "", "error"))
        return True

    def handle_failure(self, value: V, target: T, result: T) -> bool:
        self._test_results.append((value, target, result, False))
        return True

    def handle_partial_success(self):
        self.display_table()
        print(
            f"Vérifie ta fonction '{self._func_name}', au moins un test n'est pas passé...",
            file=sys.stderr,
        )

    def handle_success(self, value: V, target: T, result: T) -> None:
        self._test_results.append((value, target, result, True))

    def handle_full_success(self):
        self.display_table()
        super().handle_full_success()
