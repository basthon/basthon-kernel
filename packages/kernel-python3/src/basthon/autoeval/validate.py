import inspect
import __main__
from typing import Type, Optional

__author__ = "Romain Casati"
__license__ = "GNU GPL v3"
__email__ = "romain.casati@basthon.fr"

__all__ = [
    "validationclass",
    "Validate",
]


# the set of classes decorated with the @validationclass decorator
_decorated_classes = set()


class Validate:
    """The base class for auto-evaluation.

    You should use the :func:`@validationclass <validationclass>`
    decorator to implement your own validation process.
    It automatically makes your class derives
    from :class`Validate`. Otherwise, you could directly use a dedicated
    class like :class:`ValidateFunction` or :class:`ValidateVariables`.

    In the context of a sequenced notebooks, it implements
    the breakpoint cell validation process in the :func:`__call__` method.
    This method should return ``True`` in order to validate a cell and
    display all the cells until the next breakpoint cell.

    It is important to create a new :class:`Validate` object for each
    breakpoint code cell. Two breakpoint code cells can't share the
    same :class:`Validate` object.

    The default implementation of :func:`__call__` returns ``True``.
    This is useful for automatic breakpoint validation.
    """

    def __init__(self, ignore_breakpoint=False):
        """
        :param ignore_breakpoint: if set to True in the context of
            a sequenced notebook, we do not go to the next breakpoint
            after a success. Useful to have several tests in the same cell.
        """
        self._validated: bool = False
        self._trial_count: int = 0
        self._ignore_breakpoint: bool = ignore_breakpoint
        # use of @validationclass decorator is mandatory
        if self.__class__ not in _decorated_classes:
            raise RuntimeError(
                "You derived from Validate without using the @validationclass decorator"
            )

    def ignore_breakpoint(self, value: bool = True):
        """Enable/disable the pass through breakpoint process in a
        sequenced notebook."""
        self._ignore_breakpoint = value

    def trial_count(self) -> int:
        """Number of validation attempts i.e. number of call to :func:`__call__`."""
        return self._trial_count

    def _increment_trial_count(self):
        """Increment by one the number of validation attempts."""
        self._trial_count += 1

    def _success(self) -> None:
        """Useful only in the context of a sequenced notebook:
        it displays the remaining notebook part up to the next breakpoint cell.

        Future calls have no effect: validation can ocure only once.
        """
        # to prevent toplevel from calling _success, we ensure that
        # the (self) caller is us, from one of our methods
        stack = inspect.stack()
        frame = stack[1].frame
        caller_obj = frame.f_locals.get("self")
        if id(caller_obj) != id(self) or not hasattr(self, frame.f_code.co_name):
            raise RuntimeError(
                "Calling private method from outside the class is forbidden."
            )

        if not self._validated:
            self._validated = True
            if not self._ignore_breakpoint:
                try:
                    import js

                    js.basthon.breakpointMoveOn()
                except Exception:
                    pass

    def __call__(self) -> bool:
        """The default implementation calls succeeded without any check."""
        return True


def validationclass(theclass) -> Type[Validate]:
    """Use this decorator on all your validation classes.

    It makes it work in the context of a sequenced notebook,
    automatically implements the trial counter, verify it inherit
    from :class:`Validate`, ...
    """
    if not issubclass(theclass, Validate):
        _theclass = theclass

        class Decorated(_theclass, Validate):
            def __init__(self, *args, **kwargs):
                forwarded_kwargs = ["ignore_breakpoint"]
                validate_kwargs = {
                    arg: kwargs[arg] for arg in forwarded_kwargs if arg in kwargs
                }
                Validate.__init__(self, **validate_kwargs)
                _theclass.__init__(self, *args, **kwargs)

        Decorated.__name__ = _theclass.__name__
        Decorated.__doc__ = _theclass.__doc__
        Decorated.__init__.__doc__ = _theclass.__init__.__doc__

        theclass = Decorated

    base_call = getattr(theclass, "__call__")

    def __call__(self, *args, **kwargs) -> Optional[bool]:
        """Increment the trial count and call :func:`~Validate._success`
        in case of success."""

        # increment trial counter if needed
        def incr_if_needed(trial_count=self.trial_count()):
            if trial_count == self.trial_count():
                self._increment_trial_count()

        try:
            res = base_call(self, *args, **kwargs)
        except Exception:
            incr_if_needed()
            raise
        incr_if_needed()
        if res:
            self._success()
        # do not return result if called from toplevel
        stack = inspect.stack()
        frame = stack[1].frame
        caller_obj = frame.f_locals.get("self")
        if id(caller_obj) == id(self) and hasattr(self, frame.f_code.co_name):
            return res

    # a marker to detect class decoration
    decoration_marker = "__validation_decorated__"

    # decorating only if needed
    if not hasattr(base_call, decoration_marker):
        __call__.__doc__ = base_call.__doc__
        setattr(__call__, decoration_marker, True)
        setattr(theclass, "__call__", __call__)

    # remember we decorate this class
    _decorated_classes.add(theclass)

    return theclass


# Validate is decorated as a validationclass
Validate = validationclass(Validate)
