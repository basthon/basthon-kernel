from typing import Any
import sys
import __main__
from .validate import validationclass
from .sequences import ValidateSequences

__author__ = "Romain Casati"
__license__ = "GNU GPL v3"
__email__ = "romain.casati@basthon.fr"

__all__ = [
    "ValidateVariables",
]


@validationclass
class ValidateVariables(ValidateSequences[str, Any]):
    """A class to validate user defined variables.

    To customize this class, have a look at :class:`ValidateSequences`.
    """

    def __init__(self, names_and_values: dict[str, Any], **kwargs):
        """
        :param names_and_values: a dict with variable names as key
            and desired values as values to test against variables
            defined in the user namespace (top-level).
        :param \**kwargs: passed to parent constructor.
        """
        super().__init__(names_and_values.keys(), names_and_values.values(), **kwargs)

    def compute_result(self, value: str, precomputed_data: Any) -> Any:
        return getattr(__main__, value)

    def handle_failure(self, name: str, target: Any, value: Any) -> bool:
        print(
            f"Vérifie ta variable '{name}', elle n'a pas la bonne valeur.",
            file=sys.stderr,
        )
        return False

    def handle_exception(self, exc: Exception, name: str, target: Any) -> bool:
        print(
            f"La variable '{name}' n'est pas définie, as-tu validé la cellule ?",
            file=sys.stderr,
        )
        return False

    def handle_full_success(self):
        if len(self._values) > 1:
            print("👏 Bravo, tes variables sont bien définies !")
        elif len(self._values) == 1:
            print(f"👏 Bravo, ta variable '{self._values[0]}' est bien définie !")
