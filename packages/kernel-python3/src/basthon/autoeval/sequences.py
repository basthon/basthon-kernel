import numbers
import math
from typing import TypeVar, Generic, Iterable, Any
from .validate import validationclass, Validate

__author__ = "Romain Casati"
__license__ = "GNU GPL v3"
__email__ = "romain.casati@basthon.fr"

__all__ = ["ValidateSequences", "V", "T"]


V = TypeVar("V")
T = TypeVar("T")


def equality(
    a: T,
    b: T,
    rel_tol: float = 1e-9,
    abs_tol: float = 0.0,
) -> bool:
    """Test the equality between ``a`` and ``b`` by carrefuly
    looking at the type. Float equality is done by
    :func:`math.isclose` using ``rel_tol`` and ``abs_tol``.

    :param a: fist value to compare.
    :param b: second value to compare.
    :param rel_tol: used only for float comparison, see :func:`math.isclose`.
    :param abs_tol: used only for float comparison, see :func:`math.isclose`.
    """
    if (
        isinstance(a, type(b))
        or isinstance(b, type(a))
        or isinstance(a, numbers.Number)
        and isinstance(b, numbers.Number)
    ):
        if type(a) is float or type(b) is float:
            return math.isclose(a, b, rel_tol=rel_tol, abs_tol=abs_tol)
        else:
            try:
                return a == b
            except Exception:
                return False
    else:
        return False


@validationclass
class ValidateSequences(Generic[V, T], Validate):
    """Validate the equality between targets and results associated
    with values.
    This is the base class for :class:`ValidateFunction` and
    :class:`ValidateVariables`. It is not intended to be used directly
    but to be derived.

    The testing process is launched by calling the object.
    """

    def __init__(self, values: Iterable[V], targets: Iterable[T], **kwargs):
        """Initialise inputs and disered outputs.
        Raise ``ValueError`` if values and targets differ in length.

        :param values: input values used to compute results using
            :func:`~compute_result`
        :param targets: the corresponding target results
        :param \**kwargs: passed to parent constructor."""
        super().__init__(**kwargs)
        self._values: list[V] = list(values)
        self._targets: list[T] = list(targets)
        if len(self._values) != len(self._targets):
            raise ValueError("Arguments `values` and `targets` should have same size")

    def equality(
        self,
        a: T,
        b: T,
        rel_tol: float = 1e-9,
        abs_tol: float = 0.0,
    ) -> bool:
        """Test the equality by carrefuly looking at the type.
        Default implementation uses :func:`~.equality`.
        """
        return equality(a, b, rel_tol, abs_tol)

    def handle_failure(self, value: V, target: T, result: T) -> bool:
        """This method is called when the ``result`` computed by
        :func:`compute_result` using ``value`` is different from
        the ``target``. It is intended to be overridden.

        :param value: the value that caused the error
        :param target: the corresponding target
        :param result: the computed result
        :return: Indicate whether we continue the testing process
            or not. If the return value is ``True``, the testing
            process continue, otherwise, it stops."""
        return True

    def handle_exception(self, exc: Exception, value: V, target: T) -> bool:
        """This method is called when the call of :func:`compute_result` on
        ``value`` raises an exception. It is intended to be overridden.

        :param exc: the raised exception
        :param value: the value that caused the error
        :param target: the corresponding target
        :return: Indicate whether we continue the testing process
            or not. If the return value is ``True``, the testing
            process continue, otherwise, it stops."""
        return True

    def handle_partial_success(self):
        """This method is called at the end of the testing process,
        when at least one error occurred (different target and result
        or exception raised). It is intended to be overridden.
        """
        pass

    def handle_success(self, value: V, target: T, result: T) -> None:
        """This method is called when the ``result`` computed by
        :func:`compute_result` using ``value`` is equal to
        the ``target`` (wrt. :func:`equality`).
        It is intended to be overridden.

        :param value: the value used to compute the result
        :param target: the corresponding target
        :param result: the computed result
        """
        pass

    def handle_full_success(self):
        """This method is called at the end of the testing process,
        when all tests succeeded. It is intended to be overridden.
        """
        pass

    def precompute(self) -> Any:
        """Precompute data before a test session.
        It is intended to be overridden.

        :return: Any precomputed data that should be passed to
            :func:`compute_result`.
        """
        pass

    def handle_precompute_exception(self, exc: Exception) -> None:
        """Handle an exception occurring during the ``precompute`` process.

        :param exc: the exception raise by the call to ``precompute``."""
        pass

    def compute_result(self, value: V, precomputed_data: Any) -> T:
        """Computes the result corresponding to ``value``.

        :param value: the value used to compute the desired result
        :param precomputed_data: data returned by :func:`precompute`
        :return: The result corresponding to ``value``.
        """
        pass

    def __call__(self) -> bool:
        """Starts the validation process by testing results computed
        from values against targets."""
        self._increment_trial_count()
        failure = False
        try:
            precomputed_data = self.precompute()
        except Exception as exc:
            self.handle_precompute_exception(exc)
            return False
        for value, target in zip(self._values, self._targets):
            try:
                res = self.compute_result(value, precomputed_data)
            except Exception as exc:
                failure = True
                if self.handle_exception(exc, value, target):
                    continue
                else:
                    break
            if self.equality(res, target):
                self.handle_success(value, target, res)
            else:
                failure = True
                if not self.handle_failure(value, target, res):
                    break
        if failure:
            self.handle_partial_success()
        else:
            self.handle_full_success()
        return not failure
