"""
This is the Python part of the Basthon Kernel.
"""

try:
    import js
    from pyodide.ffi import to_js, JsProxy

    inside_pyodide = True
except ImportError:
    # we are probably importing this from sphinx
    inside_pyodide = False

__author__ = "Romain Casati"
__license__ = "GNU GPL v3"
__email__ = "romain.casati@basthon.fr"

from typing import Union, Optional, Any

if inside_pyodide:
    import os
    import sys
    import importlib
    import asyncio
    from ._console import InteractiveConsole
    from . import _patch_modules

    # where we put user supplied modules
    _user_modules_root = "/basthon_user_modules"
    # we don't insert at position 0 since
    # https://github.com/iodide-project/pyodide/issues/737#issuecomment-750858417
    sys.path.insert(2, _user_modules_root)
    # can't figure out why this is needed...
    importlib.invalidate_caches()

    # interpretation system is the Pyodide's interactive console
    _console = InteractiveConsole()

    # copying methods from _console to this module
    for f in ("eval", "complete", "banner", "more"):
        globals()[f] = getattr(_console, f)


def locals():
    """Global evaluation namespace."""
    return _console.locals


def display_event(data):
    """Dispatching eval.display event with display data."""
    js.basthon.display(to_js(data, dict_converter=js.Object.fromEntries))


async def input_async(prompt=None, password=False):
    """Dispatching eval.input event with data."""
    if js.basthon.syncCommSupport():
        raise RuntimeError(
            "Trying to use legacy input inside kernel that supports sync communication."
        )
    eval_data = _console.locals["__eval_data__"]
    eval_data = to_js(eval_data, dict_converter=js.Object.fromEntries)
    return await js.basthon.toplevelInput(prompt, False, eval_data)


async def sleep_async(secs):
    """Suspend execution for secs seconds."""
    await asyncio.sleep(secs)


def clear_output(wait=True):
    js.basthon.clearOutput(wait)


def format_repr(obj):
    """Format data to support different repr types."""
    res = {"text/plain": repr(obj)}
    mimes = {
        "text/html": "_repr_html_",
        "image/svg+xml": "_repr_svg_",
        "image/png": "_repr_png_",
        "text/latex": "_repr_latex_",
        "text/markdown": "_repr_markdown_",
    }
    for mime, _repr in mimes.items():
        if hasattr(obj, _repr):
            try:
                representation = getattr(obj, _repr)()
                if representation is not None:
                    res[mime] = representation
            except Exception:
                pass
    return res


def display(
    *objs: Any,
    include: Union[None, list, tuple, set] = None,
    exclude: Union[None, list, tuple, set] = None,
    metadata: Optional[dict] = None,
    transient: Optional[dict] = None,
    display_id: Union[None, str, bool] = None,
    raw: Optional[bool] = False,
    clear: Optional[bool] = False,
    **kwargs,
):
    """Display a Python object in all frontends.
    By default all representations will be computed and sent to the frontends.
    Frontends can decide which representation is used and how.
    In terminal IPython this will be similar to using :func:`print`, for use in richer
    frontends see Jupyter notebook examples with rich display logic.

    :param \*objs: The Python objects to display.
    :param raw: Are the objects to be displayed already mimetype-keyed dicts of raw display data,
        or Python objects that need to be formatted before display? [default: False]
    :param include: A list of format type strings (MIME types) to include in the
        format data dict. If this is set *only* the format types included
        in this list will be computed.
    :param exclude: A list of format type strings (MIME types) to exclude in the format
        data dict. If this is set all format types will be computed,
        except for those included in this argument.
    :param metadata: A dictionary of metadata to associate with the output.
        mime-type keys in this dictionary will be associated with the individual
        representation formats, if they exist.
    :param transient: A dictionary of transient data to associate with the output.
        Data in this dict should not be persisted to files (e.g. notebooks).
    :param display_id: : Set an id for the display.
        This id can be used for updating this display area later via update_display.
        If given as `True`, generate a new `display_id`
    :param clear: Should the output area be cleared before displaying anything? If True,
        this will wait for additional output before clearing. [default: False]
    :param \**kwargs: Additional keyword-arguments are passed through to the display publisher.
    """
    for obj in objs:
        if not raw:
            obj = format_repr(obj)
        display_event(obj)


def download(filename: str, data: Optional[Union[str, bytes]] = None):
    """
    Download a file from the local filesystem or from submitted data.

    :param filename: path to the file to download if ``data`` is ``None``
                     otherwise, name to display inside dialog download box.
    :param data: data to download or ``None`` if ``filename`` is a path
                 to the desired file to download.

    Usage:
        >>> download("path_to_file")
        >>> download("filename", data)
    """
    if data is None:
        data = get_file(filename)
        _, filename = os.path.split(filename)
    if isinstance(data, str):
        data = data.encode()
    js.basthon.download(to_js(data), filename)


def put_file(filepath, content):
    """
    Put a file on the (emulated) local filesystem.
    """
    if isinstance(content, JsProxy):
        content = content.to_py()
    dirname, _ = os.path.split(filepath)
    if dirname:
        os.makedirs(dirname, exist_ok=True)

    with open(filepath, "wb") as f:
        f.write(content)


async def put_module(filename, content):
    """
    Put a module (*.py file) on the (emulated) local filesystem
    """
    if isinstance(content, JsProxy):
        content = content.to_py()
    source = content.tobytes().decode()
    await js.pyodide.loadPackagesFromImports(source)

    _, fname = os.path.split(filename)
    module_path = os.path.join(_user_modules_root, fname)
    put_file(module_path, content)
    # modules with '_' prefix are compiled to pyc
    if filename.startswith("_"):
        import py_compile

        py_compile.compile(module_path, cfile=f"{module_path}c")
        os.unlink(module_path)
    file_finder = sys.path_importer_cache.get(_user_modules_root)
    if file_finder is None:
        # can't figure out why this is needed...
        importlib.invalidate_caches()
    else:
        file_finder.invalidate_caches()


def user_modules():
    """
    List modules launched via put_module.
    """
    if not os.path.exists(_user_modules_root):
        return []
    return [
        f
        for f in os.listdir(_user_modules_root)
        if f.endswith(".py") or f.endswith(".pyc")
    ]


def get_file(filepath):
    """
    Get a file content from the (emulated) local filesystem.
    """
    with open(filepath, "rb") as fd:
        return fd.read()


def get_user_module_file(filename):
    """
    Get a module content (*.py) put in the user modules directory
    (via put_module).
    """
    return get_file(os.path.join(_user_modules_root, filename))


def list_basthon_modules(pure_basthon=False):
    """
    List modules provided by Pyodide and Basthon from repodata.json.

    if `pure_basthon` is True, return modules added by Basthon.
    """

    if pure_basthon:
        packages = js.pyodide._api.repodata_packages.to_py()
        root_url = js.basthon.basthonModulesRoot()
        packages = [
            p["name"] for p in packages.values() if p["file_name"].startswith(root_url)
        ]
    else:
        packages = js.pyodide._api._import_name_to_package_name.to_py()
        packages = [p for p in packages.keys() if "." not in p] + ["js"]
    return packages


def importables():
    """List of all importable modules."""
    import pkgutil

    from_sys = set(x for x in sys.modules.keys() if "." not in x)
    from_pkgutil = set(p.name for p in pkgutil.iter_modules())
    from_basthon = set(list_basthon_modules())
    return sorted(from_sys.union(from_pkgutil, from_basthon))


def start():
    return _console.start()


def stop():
    return _console.stop()


def restart():
    return _console.restart()


def execution_count():
    return _console.execution_count
