import ast
import inspect
from pyodide.console import CodeRunner


__author__ = "Romain Casati"
__license__ = "GNU GPL v3"
__email__ = "romain.casati@basthon.fr"

__all__ = []


class StringCall(ast.NodeTransformer):
    """Easter egg proposed by 2023-2024 NSI students
    at Henri Brisson, Vierzon: "hello"(print) -> print("hello")"""

    def visit_Call(self, node):
        if (
            isinstance(node.func, ast.Constant)
            and isinstance(node.func.value, str)
            and len(node.args) == 1
        ):
            node.func, node.args[0] = node.args[0], node.func
        return node


class OneAndOneIsThree(ast.NodeTransformer):
    """Easter egg proposed by 2023-2024 NSI students
    at Henri Brisson, Vierzon: 1 + 1 -> 1 + 2"""

    def visit_BinOp(self, node):
        if (
            isinstance(node.op, ast.Add)
            and isinstance(node.left, ast.Constant)
            and node.left.value == 1
            and isinstance(node.right, ast.Constant)
            and node.right.value == 1
        ):
            node.right.value = 2
        return node


# monkey-patch pyodide.console.CodeRunner to modify ast before compile
_unpatched_compile = CodeRunner.compile


def patched_compile(self, *args, **kwargs):
    for value in globals().values():
        if inspect.isclass(value) and issubclass(value, ast.NodeTransformer):
            value().visit(self.ast)
    return _unpatched_compile(self, *args, **kwargs)


CodeRunner.compile = patched_compile
