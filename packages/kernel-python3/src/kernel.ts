import { KernelMainBase } from "@basthon/kernel-base/worker";
import { Python3KernelWorker } from "./worker";

/**
 * A Python kernel that satisfies Basthon's API.
 */
export class Python3Kernel extends KernelMainBase<Python3KernelWorker> {
  constructor(options: any) {
    super(options);
  }

  protected newWorker(): Worker {
    return new Worker(new URL("./comlink-worker.js", import.meta.url), {
      //@ts-ignore
      sandboxed: true, // removing this line will cause security issues
    });
  }

  protected async importLegacyWorker(): Promise<void> {
    await import("./comlink-worker.js");
  }

  public language() {
    return "python3";
  }

  public languageName() {
    return "Python 3";
  }

  public moduleExts() {
    return ["py"];
  }

  /**
   * List content of directory (Python's virtual FS).
   */
  public async readdir(path: string): Promise<string[]> {
    if (this.remote == null) throw new Error("Can't access remote kernel!");
    return await this.remote.readdir(path);
  }

  /**
   * Change current directory (Python's virtual FS).
   */
  public async chdir(path: string): Promise<void> {
    if (this.remote == null) throw new Error("Can't access remote kernel!");
    return await this.remote.chdir(path);
  }

  /**
   * Create directory (Python's virtual FS).
   */
  public async mkdir(path: string): Promise<void> {
    if (this.remote == null) throw new Error("Can't access remote kernel!");
    return await this.remote.mkdir(path);
  }

  /**
   * List modules launched via putModule.
   */
  public async userModules(): Promise<string[]> {
    if (this.remote == null) throw new Error("Can't access remote kernel!");
    return await this.remote.userModules();
  }

  /**
   * Get a file content from the VFS.
   */
  public async getFile(path: string): Promise<Uint8Array> {
    if (this.remote == null) throw new Error("Can't access remote kernel!");
    return await this.remote.getFile(path);
  }

  /**
   * Get a user module file content.
   */
  public async getUserModuleFile(filename: string): Promise<Uint8Array> {
    if (this.remote == null) throw new Error("Can't access remote kernel!");
    return await this.remote.getUserModuleFile(filename);
  }

  /**
   * Mimic the CPython's REPL banner.
   */
  public banner() {
    /* We don't return this.__kernel__.banner();
     * since the banner should be available ASAP.
     * In tests, we check this.banner() ===  this.__kernel__.banner().
     */
    return `Python 3.11.2 (main, May  3 2023 04:00:05) on WebAssembly/Emscripten\nType \"help\", \"copyright\", \"credits\" or \"license\" for more information.`;
  }
}
