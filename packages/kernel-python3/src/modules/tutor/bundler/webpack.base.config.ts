import { Configuration } from "webpack";
import path from "path";
import "webpack-dev-server";

const rootPath = path.resolve(__dirname, "..");
const buildPath = path.join(rootPath, "extern");

const config: Configuration = {
  entry: "./src/main.ts",
  output: {
    filename: "bundle.js",
    chunkFilename: "[name].[contenthash].js",
    assetModuleFilename: "[hash][ext][query]",
    path: buildPath,
    clean: true,
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: "asset/resource",
      },
    ],
  },
  resolve: {
    extensions: [".ts", ".js"],
  },
  devServer: {
    static: {
      directory: buildPath,
    },
    devMiddleware: {
      writeToDisk: true,
    },
    compress: true,
    port: 8888,
  },
};

export default config;
