declare global {
  interface Window {
    jQuery: any;
    $: any;
  }
}

// global jquery
import jQuery from "jquery";
window.jQuery = window.$ = jQuery;

import "jquery-ui/dist/jquery-ui.js";

import "d3/d3.v2.js";
import "jquery-simplemodal/src/jquery.simplemodal.js";
import "jquery-bbq/jquery.ba-bbq.js";

//@ts-ignore
import jsp from "!!raw-loader!./jquery.jsPlumb-1.3.10-all-min.js";
eval.call(null, jsp);

//@ts-ignore
import pytutor from "!!raw-loader!./pytutor.js";
eval.call(null, pytutor);

/* css */
import "jquery-ui/dist/themes/base/jquery-ui.css";
import "./pytutor.css";
