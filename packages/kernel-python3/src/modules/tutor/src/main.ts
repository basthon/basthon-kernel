import hljs from "highlight.js";

// css
import "./pytutor-all";
import "./highlight.css";
import "./main.css";

declare global {
  interface Window {
    tutor_trace: any;
    ExecutionVisualizer: any;
  }
}

const domLoad = async (): Promise<void> => {
  await new Promise<void>((resolve) => {
    if (document.readyState === "loading") {
      const callback = () => {
        document.removeEventListener("DOMContentLoaded", callback);
        resolve();
      };
      document.addEventListener("DOMContentLoaded", callback);
    } else {
      // `DOMContentLoaded` has already fired
      resolve();
    }
  });
};

/* setup communication with parent dom for auto resizing */
const autoResize = async (
  onresize?: () => void,
  onrefresh?: () => void,
): Promise<() => void> => {
  let port: MessagePort | undefined;

  const enforceSize = () => {
    onresize?.();
    const width = document.body.scrollWidth;
    const height = document.body.scrollHeight;
    port?.postMessage?.({ type: "size", content: { width, height } });
  };

  await new Promise<void>((resolve) => {
    const setupComm = (event: MessageEvent<any>): void => {
      if (event.data instanceof MessagePort) {
        window.removeEventListener("message", setupComm);
        port = event.data as MessagePort;
        port.onmessage = (event: MessageEvent): void => {
          const type: string | undefined = event.data?.type;
          // message of type 'refresh' from parent triggers a full redraw
          // (usefull when iframe is added to a non visible parent)
          if (type === "refresh") onrefresh?.();
        };
        resolve();
      }
    };
    window.addEventListener("message", setupComm);
  });

  await domLoad();

  const o = new ResizeObserver(enforceSize);
  o.observe(document.body);

  return enforceSize;
};

/* managing resize stuff */
let tutorViz: any;
const redraw = () => tutorViz?.redrawConnectors?.();
await autoResize(redraw, () => tutorViz?.updateOutput?.());

// after autoResize, DOM is loaded

/* setup main continer */
const container = document.createElement("div");
const tutorId = "main";
container.setAttribute("id", tutorId);
document.body.appendChild(container);

tutorViz = new window.ExecutionVisualizer(tutorId, window.tutor_trace, {
  embeddedMode: false,
  // this seems useless
  /* heightChangeCallback: redraw, */
});
// added only to redraw connectors when resizing print textarea
// @ts-ignore
$(window).on("resize", redraw);

/* code highlighting in tutor */
for (const elem of document.querySelectorAll("td.cod")) {
  const text = (elem as HTMLElement).innerText;
  elem.innerHTML = `<pre><code class="language-python">${text}</code></pre>`;
}
hljs.highlightAll();
