"""
PythonTutor port to Basthon.

For privacy concern, it avoid sending user code to PythonTutor
(iframe embeding). Instead, it generates PythonTutor's traces with Basthon's
interpreter then create a PythonTutor vizualiser.
"""

import js
import pkg_resources
from urllib.parse import quote
import json
from .generate_trace import generate_trace
from basthon import kernel
import re
import builtins
from pyodide.ffi import to_js as _to_js


__author__ = "Romain Casati"
__license__ = "GNU GPL v3"
__email__ = "romain.casati@basthon.fr"


__all__ = ["tutor", "clear_inputs"]


# we log input results to pass it to python-tutor
# WARNING: in the presence of input, the tutor() call shoud be put at the end.
# WARNING #2: this input monkey patch is in conflict with the
#             async/await input monkey patch in kernel._patch_builtins.
_input_log = []
_builtin_input = input


def _input(*args, **kwargs):
    res = _builtin_input(*args, **kwargs)
    _input_log.append(res)
    return res


builtins.input = _input


def clear_inputs():
    """Clear all previous call to builtin input."""
    _input_log.clear()


# get iframe.html
with open(pkg_resources.resource_filename("tutor", "iframe.html")) as f:
    iframe_template = f.read()


def tutor(**kwargs):
    """
    Tutor the current executed script with PythonTutor.

    See PythonTutor at http://pythontutor.com for available options.

    The code is not executed by PythonTutor itself (server side)
    but by Basthon. The solution is actually entirely executed
    in frontend.
    """
    code = kernel.locals()["__eval_data__"]["code"]
    lines = code.split("\n")

    # listing tutor import lines then remove
    # this is hacky... and should be implemented with inspect or tokenize
    regex_comment = re.compile(r"^([^#]*)#(.*)$")
    import_lines = [
        i
        for i, line in enumerate(lines)
        if regex_comment.sub(r"\1", line).rstrip()
        in ("import tutor", "from tutor import tutor", "from tutor import *")
    ]
    for index in import_lines[::-1]:
        del lines[index]

    # listing tutor() call lines
    # this is hacky... and should be implemented with inspect or tokenize
    tutor_lines = [
        i
        for i, line in enumerate(lines)
        if line.startswith("tutor.tutor(") or line.startswith("tutor(")
    ]
    # actually it could be everywhere but not nested
    if not tutor_lines:
        raise RuntimeError("tutor.tutor() should be called on first or last line.")
    # removing tutor() call lines
    for index in tutor_lines[::-1]:
        del lines[index]

    # rebuild code
    code = "\n".join(lines).strip("\n")

    # generate PythonTutor trace
    trace = json.dumps(generate_trace(code, input=json.dumps(_input_log), **kwargs))
    # empty input logs
    clear_inputs()

    # building pytutor visualizer
    iframe = iframe_template.format(
        tutor_trace=repr(trace),
        extern_url=f"{js.basthon.basthonModulesRoot()}/extern/tutor",
    )

    # sending html code to Basthon for rendering
    kernel.display_event({"text/dynamic-iframe": iframe})
