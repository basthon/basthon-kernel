"""
This is not the real requests Python module
but a partial API implementation for Basthon.
"""

import js
from urllib.parse import urlencode
from collections.abc import Mapping
from .exceptions import ConnectionError

__author__ = "Romain Casati"
__license__ = "GNU GPL v3"
__email__ = "romain.casati@basthon.fr"


__all__ = ["request"]


class Headers(dict):
    """Case insensitive dict build from request"""

    def __init__(self, req):
        for h in req.getAllResponseHeaders().rstrip().split("\r\n"):
            h = h.split(":")
            self[h[0]] = ":".join(h[1:]).lstrip()

    def __setitem__(self, key, value):
        super(Headers, self).__setitem__(key.lower(), value)

    def __getitem__(self, key):
        return super(Headers, self).__getitem__(key.lower())


basestring = (str, bytes)


def to_key_val_list(value):
    """Take an object and test to see if it can be represented as a
    dictionary. If it can be, return a list of tuples, e.g.,

    ::

        >>> to_key_val_list([('key', 'val')])
        [('key', 'val')]
        >>> to_key_val_list({'key': 'val'})
        [('key', 'val')]
        >>> to_key_val_list('string')
        Traceback (most recent call last):
        ...
        ValueError: cannot encode objects that are not 2-tuples

    :rtype: list
    """
    if value is None:
        return None

    if isinstance(value, (str, bytes, bool, int)):
        raise ValueError("cannot encode objects that are not 2-tuples")

    if isinstance(value, Mapping):
        value = value.items()

    return list(value)


def _encode_params(data):
    """Encode parameters in a piece of data.

    Will successfully encode parameters when passed as a dict or a list of
    2-tuples. Order is retained if data is a list of 2-tuples but arbitrary
    if parameters are supplied as a dict.
    """

    if isinstance(data, (str, bytes)):
        return data
    elif hasattr(data, "read"):
        return data
    elif hasattr(data, "__iter__"):
        result = []
        for k, vs in to_key_val_list(data):
            if isinstance(vs, basestring) or not hasattr(vs, "__iter__"):
                vs = [vs]
            for v in vs:
                if v is not None:
                    result.append(
                        (
                            k.encode("utf-8") if isinstance(k, str) else k,
                            v.encode("utf-8") if isinstance(v, str) else v,
                        )
                    )
        return urlencode(result, doseq=True)
    else:
        return data


class Response(object):
    """See requests.Response API"""

    def __init__(self, req):
        self._req = req  # only for debuging purpose
        self.url = req.responseURL
        self.headers = Headers(req)
        self.status_code = req.status
        self.reason = req.statusText
        self.ok = self.status_code < 400
        self.cookies = None
        content_type = self.headers.get("content-type", "")
        self.encoding = next(
            (
                x.replace("charset=", "").replace(" ", "")
                for x in content_type.split(";")
                if "charset=" in x
            ),
            None,
        )
        # throw away high-order bytes (& 255)
        self.content = bytes([ord(c) & 255 for c in req.response])
        try:
            self.text = self.decode(self.encoding)
        except Exception:
            self.text = req.responseText

    def decode(self, charset="utf-8"):
        if charset is None:
            return self.content.decode()
        else:
            return self.content.decode(charset)

    def json(self, charset="utf-8", **kwargs):
        import json

        return json.loads(self.decode(charset), **kwargs)


def request(method, url, **kwargs):
    """See requests.request"""
    # url params
    if "params" in kwargs:
        url += "?" + "&".join((f"{k}={v}" for k, v in kwargs["params"].items()))
    req = js.eval(
        "var __basthon_req = new XMLHttpRequest(); __basthon_req"
    )  # hack for using js try/catch (see below)
    req.open(method.lower(), url, False)  # synchronous request
    req.overrideMimeType("text/plain; charset=x-user-defined")

    # headers
    req.setRequestHeader("accept", "*/*")
    if "data" in kwargs:
        req.setRequestHeader("content-type", "application/x-www-form-urlencoded")
    for k, v in kwargs.get("headers", {}).items():
        req.setRequestHeader(k, v)

    def send():
        req.send(_encode_params(kwargs.get("data")))

    def onerror(error):
        cors_message = ""
        url_origin = js.URL.new(url).origin
        origin = js.window.location.origin
        if url_origin != origin:
            cors_message = (
                " Requested URL is not on this domain. You may have CORS issue."
            )
        raise ConnectionError(
            f"{error.js_error.toString()}{cors_message} Please check error message in browser console."
        )

    req._patched_send = send
    req._patched_onerror = onerror
    # big hack since try/catch of JS code is not available on Python side
    js.eval(
        "try { __basthon_req._patched_send(); } catch (e) { __basthon_req._patched_onerror(e); }"
    )
    return Response(req)


for name in ("GET", "OPTIONS", "HEAD", "POST", "PUT", "PATCH", "DELETE"):
    __all__.append(name.lower())
    globals()[name.lower()] = lambda url, _=name, **kwargs: request(_, url, **kwargs)
