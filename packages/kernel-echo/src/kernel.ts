import { KernelBase } from "@basthon/kernel-base";

export class EchoKernel extends KernelBase {
  constructor(options: any) {
    super(options);
  }

  public language() {
    return "echo";
  }

  public languageName() {
    return "Echo";
  }

  public moduleExts() {
    return [];
  }

  public ps1() {
    return "echo> ";
  }

  public ps2() {
    return "...> ";
  }

  public async more(source: string): Promise<boolean> {
    return false;
  }

  public async evalAsync(
    code: string,
    outCallback: (_: string) => void,
    errCallback: (_: string) => void,
    data: any = null,
  ): Promise<any> {
    data.interactive = true;
    this._execution_count++;
    outCallback(code + "\n");
    return [undefined, this._execution_count];
  }
}
