import { Remote } from "comlink";
import { ProxyFetch } from "./worker/proxy";

declare global {
  interface Window {
    _remoteProxy: Remote<ProxyFetch>;
    _assetsURL?: string;
  }
}

/**
 * An error thrown by not implemented API functions.
 */
class NotImplementedError extends Error {
  constructor(funcName: string) {
    super(`Function ${funcName} not implemented!`);
    this.name = "NotImplementedError";
  }
}

/**
 * Convert an Uint8Array to a data URL.
 */
const toDataURL = async (content: Uint8Array): Promise<string> => {
  return await new Promise<string>((resolve, reject) => {
    const reader = new FileReader();
    reader.onloadend = () => {
      resolve(reader.result as string);
    };
    reader.onerror = reject;
    reader.readAsDataURL(new Blob([content]));
  });
};

/* fetch from local FS */
const fetchFromLocalFS = async (
  localfsScheme: string,
  url: string,
): Promise<Response> => {
  const prefix = localfsScheme;
  const path = url.slice(prefix.length);
  let content = null;
  const options = {
    status: 200,
    statusText: "OK",
    headers: new Headers(),
  };
  try {
    //@ts-ignore
    content = await self.basthon.getFile(path);
    const dataURL = await toDataURL(content);
    const mime = dataURL.substring(
      dataURL.indexOf(":") + 1,
      dataURL.indexOf(";"),
    );
    content = content.buffer;
    options.headers.append("Content-Type", mime);
    options.headers.append("Content-Length", content.byteLength);
  } catch (e) {
    options.status = 404;
    options.statusText = "Not Found";
  }
  return new Response(content, options);
};

/**
 * Get the origin of an url.
 */
const origin = (url: string): string => new URL(url).origin;

/**
 * Get the parent path of an url.
 */
const parent = (url: string): string => {
  const abs = new URL(url).href;
  return abs.substring(0, abs.lastIndexOf("/"));
};

/**
 * we mock the fetch function to:
 *  - redirect queries to local FS
 *  - bypass the worker's opaque origin
 */
const mockFetch = (legacy: boolean) => {
  // already mocked?
  if (self.fetch.name === "mockedFetch") return;
  const trueFetch = self.fetch;
  const assetsURL = self._assetsURL;
  async function mockedFetch(
    request: string | Request | URL,
    init?: RequestInit,
  ): Promise<Response> {
    // build new request from init
    request = new Request(request, init);
    const url = request.url;
    const localfsScheme = "filesystem:/";

    // in legacy mode, only local FS is mocked
    if (legacy) {
      if (url.startsWith(localfsScheme))
        return await fetchFromLocalFS(localfsScheme, url);
      return await trueFetch(request);
    }

    // not a true worker? -> error
    if (self.importScripts == null || assetsURL == null)
      return Response.error();

    // non-legacy mode (true isolated secure worker)

    // ensure credentials are not sent
    request = new Request(request, { credentials: "omit" });

    try {
      // requests towards localfs
      if (url.startsWith(localfsScheme)) {
        return await fetchFromLocalFS(localfsScheme, url);
      } else if (origin(url) === origin(assetsURL as string)) {
        // requests towards same origin
        // only url under the assets folder are proxyied
        if (parent(url).startsWith(assetsURL as string)) {
          const proxy = self._remoteProxy;
          const { body, options } = await proxy.fetch(request);
          return new Response(body, options);
        }
        throw new Error("Ressource is outside the proxy' scope");
      }
      return await trueFetch(request);
    } catch (e: any) {
      console.error(
        `security proxy: request to ${url} throw error: ${e.toString()}`,
      );
      return Response.error();
    }
  }
  self.fetch = mockedFetch;
};

/**
 * mock Worker to support sandboxing and keep webpack compatibility
 */
const mockWorker = (): void => {
  // already mocked?
  if (globalThis.Worker.name === "SandboxedWorker") return;
  class SandboxedWorker extends Worker {
    constructor(url: string | URL, options?: any) {
      if (options?.sandboxed === true) {
        const absURL = new URL(url).href;
        const assetsURL = absURL.substring(0, absURL.lastIndexOf("/"));
        // worker created with a 'data' url scheme has opaque origin
        // https://html.spec.whatwg.org/multipage/workers.html#script-settings-for-workers:concept-origin-opaque
        const workerScript = `
           self._assetsURL = "${assetsURL}";
           globalThis = {importScripts: self.importScripts, location: "${url}"};
           importScripts("${url}");
           globalThis = self;
        `.replace(/\s/g, "");
        super(`data:text/javascript;base64,${btoa(workerScript)}`);
      } else {
        super(url, options);
      }
    }
  }
  globalThis.Worker = SandboxedWorker;
};

/**
 * we mock '(new Image()).src' = to support local fs access
 */
const mockHTMLImageSrc = (): void => {
  // inside main thread?
  if (globalThis?.HTMLImageElement == null) return;
  const desc = Object.getOwnPropertyDescriptor(
    HTMLImageElement.prototype,
    "src",
  );

  // already mocked?
  if (desc?.set?.name === "mockedSrcSet") return;

  function mockedSrcSet(this: HTMLImageElement, e: any) {
    const prefix = "filesystem:/";
    if (e.startsWith(prefix)) {
      const path = e.slice(prefix.length);
      (async () => {
        //@ts-ignore
        const content = (await globalThis.Basthon?.getFile?.(path))?.buffer;
        desc?.set?.call(this, await toDataURL(content));
      })();
    } else {
      desc?.set?.call(this, e);
    }
  }

  Object.defineProperty(HTMLImageElement.prototype, "src", {
    ...desc,
    get: function () {
      return desc?.get?.call(this);
    },
    set: mockedSrcSet,
  });
};

export { NotImplementedError, mockFetch, mockWorker, mockHTMLImageSrc };
