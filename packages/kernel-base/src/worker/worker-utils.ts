import { expose as exposeComlink } from "comlink";

declare let self: DedicatedWorkerGlobalScope;

declare global {
  interface DedicatedWorkerGlobalScope {
    _commPort?: MessagePort; // main comlink communication port
  }
}

const insideWorker = (): boolean =>
  //@ts-ignore
  typeof WorkerGlobalScope !== "undefined" && self instanceof WorkerGlobalScope;

const expose = (kwc: any): void => {
  if (insideWorker()) {
    // worker
    const listener = (msg: any): void => {
      if (!(msg?.data instanceof MessagePort)) return;
      removeEventListener("message", listener);
      exposeComlink(kwc, msg.data);
    };
    addEventListener("message", listener);
  } else {
    // main thread
    exposeComlink(kwc, self._commPort);
    self._commPort = undefined; // no more needed
  }
};

/**
 * DOM node exchanger (Bus) to bypass stringifying
 * messages between frontend and kernel that prevents DOMNode sharing.
 */
class DOMNodeBus extends Map<number, any> {
  /**
   * Push an object to the bus and get back an id to later pop it.
   */
  public push(obj: any): number {
    let id = 0;
    for (; id < this.size; id++) if (!this.has(id)) break;
    this.set(id, obj);
    return id;
  }

  /**
   * Remove an object from the bus from its id.
   */
  public pop(id: number): any {
    const res = this.get(id);
    this.delete(id);
    return res;
  }
}

/**
 * Dynamically load a script (support main thread and worker usage).
 */
const loadScript = async (url: string): Promise<void> => {
  if (globalThis.importScripts != null) return importScripts(url);
  // we are probably in main thread
  return new Promise<any>((resolve, reject): void => {
    const script = document.createElement("script");
    script.onload = resolve;
    script.onerror = reject;
    script.src = url;
    document.head.appendChild(script);
  });
};

export { expose, DOMNodeBus, loadScript };
