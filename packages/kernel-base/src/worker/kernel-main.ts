import { wrap, Remote, createEndpoint, transfer } from "comlink";

import { NotImplementedError } from "../commons";
import { KernelWorkerBase, ExecuteResult } from "./kernel-worker";
import { KernelBase } from "../kernel";
import { ProxyFetch } from "./proxy";

// mock Worker to support sandboxing and keep webpack compatibility
if (globalThis.Worker.name !== "SandboxedWorker") {
  class SandboxedWorker extends Worker {
    constructor(url: string | URL, options?: any) {
      if (options.sandboxed) {
        const absURL = new URL(url).href;
        const assetsURL = absURL.substring(0, absURL.lastIndexOf("/"));
        // worker created with a 'data' url scheme has opaque origin
        // https://html.spec.whatwg.org/multipage/workers.html#script-settings-for-workers:concept-origin-opaque
        const workerScript = `self._assetsURL = "${assetsURL}";
           globalThis = {importScripts: self.importScripts, location: "${url}"};
           importScripts("${url}");
           globalThis = self;`.replace(/\s/g, "");
        super(`data:text/javascript;base64,${btoa(workerScript)}`);
      } else {
        super(url, options);
      }
    }
  }
  globalThis.Worker = SandboxedWorker;
}

export class KernelMainBase<Type extends KernelWorkerBase> extends KernelBase {
  // worker exposed through comlink
  private _worker?: Worker;
  private _remote?: Remote<Type>;

  constructor(options: any) {
    super(options);
    // start the kernel asap
    this.start();
  }

  /**
   * Get a new worker (should be overloaded).
   */
  protected newWorker(): Worker {
    throw new NotImplementedError("newWorker");
  }

  /**
   * Start the worker.
   */
  public async start(remoteInitOptions?: any): Promise<void> {
    await super.start();
    const worker = this.newWorker();

    // setup the proxy worker
    const proxyWorker = new Worker(
      new URL("./comlink-proxy.js", import.meta.url),
      {
        type: "module",
      }
    );
    const proxy = wrap<ProxyFetch>(proxyWorker);

    // setup the kernel worker
    this._worker = worker;
    worker.onmessage = (e) => this.processWorkerMessage(e.data);
    this._remote = wrap(worker);

    // bind ports
    const port = await proxy[createEndpoint]();
    await this._remote.setProxyPort(transfer(port, [port]));

    // init remote
    await this._remote.init(remoteInitOptions);
  }

  /**
   * Comlink remote getter.
   */
  protected get remote(): Remote<Type> | undefined {
    return this._remote;
  }

  /**
   * Stop the worker.
   */
  public async stop(): Promise<void> {
    this._worker?.terminate();
    this._worker = undefined;
    this._remote = undefined;
    await super.stop();
  }

  /**
   * Launch is basically waiting for worker to be ready.
   */
  public async launch(): Promise<void> {
    await this.remote?.ready();
  }

  /**
   * Process messages from worker (if any).
   */
  protected processWorkerMessage(msg: {
    data: any;
    type: string;
    content: any;
  }): void {
    switch (msg.type) {
      case "stream":
        const { stream, text } = msg.content as {
          stream: "stdout" | "stderr";
          text: string;
        };
        const dataEvent = this.clone(msg.data);
        dataEvent.stream = stream;
        dataEvent.content = text;
        this.dispatchEvent("eval.output", dataEvent);
        break;
      case "download":
        const { content, filename } = msg.content;
        this.download(content, filename);
        break;
      default: // ignoring (probably a comlink message)
    }
  }

  public async evalAsync(
    code: string,
    outCallback: (_: string) => void,
    errCallback: (_: string) => void,
    data: any = null
  ): Promise<any> {
    // force interactivity in all modes
    data.interactive = true;

    this._execution_count++;

    // evaluation
    const execResult = (await this.remote?.eval(data, code)) as ExecuteResult;

    // return result
    let result;
    switch (execResult?.status) {
      case "ok":
        result = execResult.result;
        break;
      case "error":
        break;
    }
    return [result, this._execution_count];
  }
}
