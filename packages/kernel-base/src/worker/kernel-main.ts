import { wrap, Remote, createEndpoint, transfer } from "comlink";

import { NotImplementedError, mockWorker, mockHTMLImageSrc } from "../commons";
import { KernelWorkerBase, ExecuteResult } from "./kernel-worker";
import { KernelBase } from "../kernel";
import { ProxyFetch } from "./proxy";
import { PromiseDelegate } from "promise-delegate";

// mock Worker to support sandboxing and keep webpack compatibility
mockWorker();

// mock HTMLImageElement.src to support localfs access
mockHTMLImageSrc();

export class KernelMainBase<Type extends KernelWorkerBase> extends KernelBase {
  // worker exposed through comlink
  private _worker?: Worker;
  private _remote?: Remote<Type>;
  private readonly _legacy: boolean;
  private postMessageToWorker?: InstanceType<typeof MessagePort>["postMessage"];
  // for synchronous communication between main thread and worker
  private _sab?: SharedArrayBuffer;
  // synchronise comlink and communication channels
  private _channelSyncer = new PromiseDelegate<void>();
  private _fallbackKey?: string;
  private _encrypt = (s: string) => s;

  constructor(options: any) {
    super(options);
    this._legacy = options?.legacy === true;
    // synchronous wait on SAB cannot be used in a legacy context
    // (atomic wait will block the main thread)
    if (!this.isLegacy()) {
      if (globalThis.crossOriginIsolated)
        this._sab = new SharedArrayBuffer(1024);
      else this._fallbackKey = globalThis.crypto.randomUUID();
    }
  }

  /**
   * Is this a legacy worker? i.e. not runing in a worker.
   */
  public isLegacy(): boolean {
    return this._legacy;
  }

  /**
   * Is synchrnous communication with worker available?
   * (through SharedArrayBuffer atomic waits)
   */
  public syncCommSupport(): boolean {
    return this._sab != null;
  }

  /**
   * Is this kernel safe? (loaded in an isolated web worker)
   */
  public safeKernel(): boolean {
    return !this.isLegacy();
  }

  /**
   * Get a new worker (should be overloaded).
   */
  protected newWorker(): Worker {
    throw new NotImplementedError("newWorker");
  }

  /**
   * Import the code for a legacy worker (running in the main thread).
   */
  protected async importLegacyWorker(): Promise<void> {
    throw new NotImplementedError("importLegacyWorker");
  }

  /**
   * Create the kernel webworker and setup communication.
   */
  protected async setupWorker(): Promise<void> {
    const comlinkChannel = new MessageChannel();
    if (this.isLegacy()) {
      // running in main thread via message channel (fake worker)
      //@ts-ignore
      self._commPort = comlinkChannel.port1;
      // this global variable will be removed by worker
      self._assetsURL = this.assetsURL(true);
      await this.importLegacyWorker();
    } else {
      // setup the kernel worker
      const worker = this.newWorker();
      // send communication port
      worker.postMessage(comlinkChannel.port1, [comlinkChannel.port1]);
      this._worker = worker;
    }

    const KernelWorker = wrap<KernelWorkerBase>(comlinkChannel.port2);
    // why typescript complains about this call? yet, this is advertised in
    // https://github.com/GoogleChromeLabs/comlink/blob/dffe9050f63b1b39f30213adeb1dd4b9ed7d2594/docs/examples/03-classes-example/index.html#L15
    //@ts-ignore
    this._remote = await new KernelWorker(this._options);

    // setup the communication channel
    const { port1, port2 } = new MessageChannel();
    port2.onmessage = (e) => this.processWorkerMessage(e.data);
    this.postMessageToWorker = port2.postMessage.bind(port2);
    await this.remote?.setCommPort(transfer(port1, [port1]));
    // WARNING: without the setCommPort call, KernelWorker (proxy)
    // is garbage collected and comlink releases the proxy,
    // breaking all the communications... (don't know really why)
    // If you experience this, just keep a ref on the proxy with e.g.
    // this.PROXY = KernelWorker
  }

  /**
   * Start the worker.
   */
  public async _start(): Promise<void> {
    await super._start();

    // kernel restart in legacy mode is lazy, not a full restart
    if (this.isLegacy() && this.remote != null) {
      await this.remote?.legacyStart();
      return;
    }

    // setup the proxy worker
    const proxyWorker = new Worker(
      new URL("./comlink-proxy.js", import.meta.url),
    );
    const proxy = wrap<ProxyFetch>(proxyWorker);

    // setup the kernel worker
    await this.setupWorker();

    if (this.syncCommSupport()) {
      console.log(
        "Using SharedArrayBuffer for synchronous worker communication.",
      );
      await this._remote?.setSAB(this._sab!);
    } else if (this._fallbackKey != null) {
      console.warn(
        "SharedArrayBuffer not available, falling back to " +
          "using third-party server for synchronous worker communication.",
      );
      const pass = globalThis.crypto.randomUUID();
      await this.remote?.setFallbackKeyAndPass(this._fallbackKey, pass);
      const AES = await import("crypto-js/aes");
      this._encrypt = (message: string): string =>
        AES.encrypt(message, pass).toString();
    }

    // bind ports
    const port = await proxy[createEndpoint]();
    await this.remote?.setProxyPort(transfer(port, [port]));

    // init remote
    await this.remote?.init();
  }

  /**
   * Comlink remote getter.
   */
  protected get remote(): Remote<Type> | undefined {
    return this._remote;
  }

  /**
   * Stop the worker.
   */
  public async _stop(): Promise<void> {
    if (this.isLegacy()) {
      await this.remote?.legacyStop();
    } else {
      this._worker?.terminate();
      this._worker = undefined;
      this._remote = undefined;
    }
    await super._stop();
  }

  /**
   * Process messages from worker (if any).
   */
  protected async processWorkerMessage(msg: {
    data: any;
    type: string;
    content: any;
  }): Promise<void> {
    switch (msg.type) {
      case "eval-end":
        this._channelSyncer.resolve();
        this._channelSyncer = new PromiseDelegate<void>();
        break;
      case "stream":
        const { stream, text } = msg.content as {
          stream: "stdout" | "stderr";
          text: string;
        };
        const dataEvent = this.clone(msg.data);
        dataEvent.stream = stream;
        dataEvent.content = text;
        this.dispatchEvent("eval.output", dataEvent);
        break;
      case "download":
        const { content, filename } = msg.content;
        this.download(content, filename);
        break;
      case "display":
        if (msg.data?.display_type === "dom-node") {
          // we should be in legacy mode here
          if (!this.isLegacy()) throw new Error("internal error");
          const id = msg.data?.content;
          //@ts-ignore
          msg.data.content = await self.basthon?.popDOMNode(id);
        }
        this.dispatchEvent("eval.display", msg.data);
        break;
      case "clear_output":
        this.dispatchEvent("eval.clear-output", msg.data);
        break;
      case "input":
        const { prompt, password, data } = msg.content;
        const res = (await this.inputAsync(prompt, password, data)) as string;
        if (msg.content.toplevel === true) {
          this.postMessageToWorker!({
            type: "toplevel-input",
            content: res,
            data,
          });
        } else if (this.syncCommSupport()) {
          const encodedRes = new TextEncoder().encode(res);
          new Uint8Array(this._sab!).set(
            encodedRes.subarray(0, this._sab!.byteLength),
          );
          Atomics.notify(new Int32Array(this._sab!), 0, 1);
        } else {
          const response = await fetch("https://api.basthon.fr/ephemeral", {
            method: "POST",
            headers: {
              "X-API-Referer": self.location.href,
            },
            body: JSON.stringify({
              action: "post",
              key: this._fallbackKey,
              payload: this._encrypt(res),
            }),
          });
          if (!response.ok)
            throw new Error(`Basthon's network API error: ${response.status}`);
        }
        break;
      case "breakpoint-move-on":
        //@ts-ignore
        const notebook = Jupyter?.notebook;
        const msg_id = msg.content.data?.parent_msg?.header?.msg_id;
        const index = notebook?.last_msg_id_to_cell_index?.(msg_id);
        notebook?.next_breakpoint_cell?.(index);
        break;
      default: // ignoring (probably a comlink message)
    }
  }

  public async evalAsync(
    code: string,
    outCallback: (_: string) => void,
    errCallback: (_: string) => void,
    data: any = null,
  ): Promise<any> {
    // force interactivity in all modes
    data.interactive = true;

    this._execution_count++;

    // to be sure we do not miss communications
    const syncChannels = this._channelSyncer.promise;

    // evaluation
    const execResult = (await this.remote?.eval(data, code)) as ExecuteResult;

    // wait for all communications to end
    await syncChannels;

    // return result
    let result;
    switch (execResult?.status) {
      case "ok":
        result = execResult.result;
        break;
      case "error":
        break;
    }
    return [result, this._execution_count];
  }

  /**
   * Put a file on the local (emulated) filesystem.
   */
  public async putFile(filename: string, content: ArrayBuffer) {
    await super.putFile(filename, content);
    await this.remote?.putFile(filename, content);
  }

  /**
   * Put an importable module on the local (emulated) filesystem
   * and load dependencies.
   */
  public async putModule(filename: string, content: ArrayBuffer) {
    await super.putModule(filename, content);
    await this.remote?.putModule(filename, content);
  }

  /**
   * Complete a code at the end (usefull for tab completion).
   *
   * Returns an array of two elements: the list of completions
   * and the start index.
   */
  public async complete(code: string): Promise<[string[], number] | []> {
    return (await this.remote?.complete(code)) ?? [];
  }

  /**
   * Tell wether we should wait for more code or if it can
   * be run as is.
   *
   * Useful to set ps1/ps2 in console prompt.
   */
  public async more(code: string): Promise<boolean> {
    return (await this.remote?.more(code)) ?? false;
  }
}
