import { PromiseDelegate } from "promise-delegate";
import { NotImplementedError, mockFetch } from "../commons";
import { wrap, Remote } from "comlink";
import { ProxyFetch } from "./proxy";

declare global {
  interface Window {
    _remoteProxy: Remote<ProxyFetch>;
  }
}

type ExecuteResult = {
  data: any;
  status: "ok" | "error";
  result?: any;
  error?: any;
};

class KernelWorkerBase {
  private _ready = new PromiseDelegate<void>();

  constructor() {
    /* we mock the fetch function to:
     *  - redirect queries to local FS
     *  - bypass the worker's opaque origin
     */
    mockFetch();
  }

  /*
   * Initialize the kernel. Should be overloaded.
   */
  protected async _init(options?: any): Promise<void> {}

  /**
   * Initialize the kernel. It is called by initWorker in KernelBase.
   */
  public async init(options?: any): Promise<void> {
    try {
      await this._init(options);
      this._ready.resolve();
    } catch (e) {
      this._ready.reject(e);
    }
  }

  /**
   * Wait for the kernel to be ready.
   */
  public ready(): Promise<void> {
    return this._ready.promise;
  }

  /**
   * Set the comlink communication port to proxy fetch.
   */
  public setProxyPort(port: MessagePort): void {
    self._remoteProxy = wrap(port);
  }

  /**
   * Send stream to stdout/stderr.
   */
  private sendStream(data: any, stream: "stdout" | "stderr", text: string) {
    self.postMessage({
      data,
      type: "stream",
      content: { stream, text },
    });
  }

  /**
   * Send stream to stdout.
   */
  protected sendStdoutStream(data: any, text: string) {
    this.sendStream(data, "stdout", text);
  }
  /**
   * Send stream to stderr.
   */
  protected sendStderrStream(data: any, text: string) {
    this.sendStream(data, "stderr", text);
  }

  /**
   * Evaluate code and return an object with mime type keys and string value.
   * To be overloaded.
   */
  protected _eval(
    data: any,
    code: string
  ): undefined | { [key: string]: string } {
    throw new NotImplementedError("_eval");
  }

  /**
   * Evaluate a string of code. To be called by main thread (through comlink).
   */
  public eval(data: any, code: string): ExecuteResult {
    try {
      const result = this._eval(data, code);
      return { data, status: "ok", result };
    } catch (e: any) {
      const { name, stack, message } = e as Error;
      this.sendStderrStream(data, e.toString());
      return {
        data,
        status: "error",
        error: {
          name: name,
          value: message,
          traceback: stack?.toString(),
        },
      };
    }
  }

  /**
   * Complete the submited code.
   */
  public complete(code: string): [string[], number] | [] {
    return [];
  }

  /**
   * Send a message to the main thread to download a file.
   */
  public download(content: Uint8Array, filename: string): void {
    self.postMessage({ type: "download", content: { content, filename } });
  }
}

export { ExecuteResult, KernelWorkerBase };
