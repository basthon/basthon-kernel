import { PromiseDelegate } from "promise-delegate";
import { NotImplementedError, mockFetch } from "../commons";
import { wrap, Remote } from "comlink";
import { VERSION } from "../version";
import { ProxyFetch } from "./proxy";
import { DOMNodeBus } from "./worker-utils";

declare let self: DedicatedWorkerGlobalScope;

declare global {
  interface DedicatedWorkerGlobalScope {
    _assetsURL?: string;
    _remoteProxy: Remote<ProxyFetch>;
    basthon: KernelWorkerBase;
    _prompt: (p?: string, _d?: string) => string | null;
    window?: DedicatedWorkerGlobalScope;
    Canvas:
      | typeof OffscreenCanvas
      | ((w: number, h: number) => HTMLCanvasElement);
  }
}

type ExecuteResult = {
  data: any;
  status: "ok" | "error";
  result?: any;
  error?: any;
};

class KernelWorkerBase {
  protected _options: any;
  protected readonly _legacy: boolean;
  private readonly _basthonRoot: string;
  private _ready = new PromiseDelegate<void>();
  protected __eval_data__: any;
  protected postMessage?: InstanceType<typeof MessagePort>["postMessage"];
  private _evalPromises: Promise<void>[] = []; // used to wait all promises emitted during computation
  private _sab?: SharedArrayBuffer;
  private _domNodeBus?: DOMNodeBus;
  private _toplevelInputPromise?: PromiseDelegate<string>;
  // in case SharedArrayBuffer are not available
  private _fallbackKey?: string;
  private _decrypt = (s: string) => s;

  constructor(options?: any) {
    this._options = options;
    this._legacy = options?.legacy === true;
    this._basthonRoot = self._assetsURL + "/" + VERSION + "/" + this.language();

    /* we mock the fetch function to:
     *  - redirect queries to local FS
     *  - bypass the worker's opaque origin
     */
    mockFetch(this.isLegacy());

    // useful for ffi
    self.basthon = this;

    // the window object is not available in a webworker...
    if (self.window == null) self.window = self;

    // common way to create a canvas in worker and legacy mode
    self.Canvas = !this.isLegacy()
      ? globalThis.OffscreenCanvas
      : function Canvas(width: number, height: number): HTMLCanvasElement {
          const canvas = document.createElement("canvas");
          if (width != null) canvas.width = width;
          if (height != null) canvas.height = height;
          return canvas;
        };

    // global prompt redirected to our input
    if (self._prompt == null) self._prompt = globalThis.prompt;
    globalThis.prompt = (p: string | undefined) => this.input(p);

    // DOM node bus in legacy mode
    // (to transfer elements from worker to main thread, they are the same!)
    if (this.isLegacy()) this._domNodeBus = new DOMNodeBus();

    // not needed anymore
    self._assetsURL = undefined;
  }

  /**
   * Kernel version (same as main thread version() method).
   */
  public version(): string {
    return VERSION;
  }

  /**
   * Language implemented in the kernel (string).
   * Generally lower case.
   */
  public language(): string {
    throw new NotImplementedError("language");
  }

  /**
   * Is this a legacy worker? i.e. not runing in a worker.
   */
  public isLegacy(): boolean {
    return this._legacy;
  }

  /**
   * The basthon's root directory.
   */
  public basthonRoot(): string {
    return this._basthonRoot;
  }

  /**
   * Is synchronous communication with main thread available?
   * (through SharedArrayBuffer atomic waits)
   */
  public syncCommSupport(): boolean {
    return this._sab != null;
  }

  /**
   * Special case of starting a legacy kernel.
   */
  public async legacyStart(): Promise<void> {
    throw new NotImplementedError("legacyStart");
  }

  /**
   * Special case of stoping a legacy kernel.
   */
  public async legacyStop(): Promise<void> {
    throw new NotImplementedError("legacyStop");
  }

  /**
   * Set SharedArrayBuffer from main thread.
   */
  public setSAB(sab: SharedArrayBuffer): void {
    this._sab = sab;
  }

  /**
   * Add a promise we should wait for during eval process.
   */
  protected addEvalPromise(promise: Promise<void>): void {
    this._evalPromises.push(promise);
  }

  /*
   * Set fallback API key and encryption pass
   * (if SharedArrayBuffer are not available).
   */
  public async setFallbackKeyAndPass(key: string, pass: string): Promise<void> {
    this._fallbackKey = key;
    const CryptoJS = await import("crypto-js");
    this._decrypt = (message: string): string =>
      CryptoJS.AES.decrypt(message, pass).toString(CryptoJS.enc.Utf8);
  }

  /*
   * Initialize the kernel. Should be overloaded.
   */
  protected async _init(): Promise<void> {}

  /**
   * Initialize the kernel. It is called by initWorker in KernelBase.
   */
  public async init(): Promise<void> {
    try {
      await this._init();
      this._ready.resolve();
    } catch (error) {
      this._ready.reject(error);
      throw error;
    }
  }

  /**
   * Wait for the kernel to be ready.
   */
  public ready(): Promise<void> {
    return this._ready.promise;
  }

  /**
   * Set communication port.
   */
  public setCommPort(port: MessagePort): void {
    this.postMessage = port.postMessage.bind(port);
    port.onmessage = (e) => this.processMainThreadMessage(e.data);
  }

  /**
   * Set the comlink communication port to proxy fetch.
   */
  public setProxyPort(port: MessagePort): void {
    self._remoteProxy = wrap(port);
  }

  /**
   * Send stream to stdout/stderr.
   */
  private sendStream(data: any, stream: "stdout" | "stderr", text: string) {
    this.postMessage?.({
      data,
      type: "stream",
      content: { stream, text },
    });
  }

  /**
   * Send stream to stdout.
   */
  protected sendStdoutStream(data: any, text: string): void {
    this.sendStream(data, "stdout", text);
  }
  /**
   * Send stream to stderr.
   */
  protected sendStderrStream(data: any, text: string): void {
    this.sendStream(data, "stderr", text);
  }

  protected async blobToDataURL(blob: Blob): Promise<string> {
    return await new Promise((resolve) => {
      const reader = new FileReader();
      reader.onloadend = () => resolve(reader.result as string);
      reader.readAsDataURL(blob);
    });
  }

  /**
   * Display a blob (only png images are supported).
   */
  protected async displayBlob(blob: Blob, data: any): Promise<void> {
    if (data == null) data = this.clone(this.__eval_data__);
    const dataURL = await this.blobToDataURL(blob);
    const pngPrefix = "data:image/png;base64,";
    if (dataURL.startsWith(pngPrefix)) {
      const png = dataURL.slice(pngPrefix.length);
      data["display_type"] = "multiple";
      data["content"] = { "image/png": png };
    } else {
      return;
    }
    this.postMessage?.({ data, type: "display" });
  }

  /**
   * Display an element like a canvas, ... in the frontend.
   */
  public display(element: any): void {
    const data = this.clone(this.__eval_data__);
    if (
      globalThis.OffscreenCanvas != null &&
      element instanceof OffscreenCanvas
    ) {
      this.addEvalPromise(
        (async () => {
          const blob = await element.convertToBlob();
          await this.displayBlob(blob, data);
        })(),
      );
    } else {
      // in legacy mode, send HTML elements via DOM node bus
      if (this.isLegacy() && element instanceof HTMLElement) {
        data["display_type"] = "dom-node";
        data["content"] = this._domNodeBus?.push?.(element);
      } else {
        data["display_type"] = "multiple";
        data["content"] = element;
      }
      this.postMessage?.({ data, type: "display" });
    }
  }

  /**
   * Clear the current output.
   */
  public clearOutput(wait: boolean): void {
    const data = this.clone(this.__eval_data__);
    data["content"] = { wait };
    this.postMessage?.({ data, type: "clear_output" });
  }

  /**
   * Pop a DOM node from domNodeBus
   * (used to transfer HTMLElement from main thread to worker thread in legacy mode)
   */
  public popDOMNode(id: number) {
    return this._domNodeBus?.pop(id);
  }

  /**
   * Evaluate code and return an object with mime type keys and string value.
   * To be overloaded.
   */
  protected async _eval(
    data: any,
    code: string,
  ): Promise<undefined | { [key: string]: string }> {
    throw new NotImplementedError("_eval");
  }

  /**
   * Evaluate a string of code. To be called by main thread (through comlink).
   */
  public async eval(data: any, code: string): Promise<ExecuteResult> {
    this._evalPromises = [];
    this.__eval_data__ = data;
    let res: ExecuteResult;
    try {
      const result = await this._eval(data, code);
      res = { data, status: "ok", result };
    } catch (e: any) {
      const { name, stack, message } = e as Error;
      this.sendStderrStream(data, e.toString());
      res = {
        data,
        status: "error",
        error: {
          name: name,
          value: message,
          traceback: stack?.toString(),
        },
      };
    }
    this.postMessage?.({ type: "eval-end" }); // for channel syncing
    await Promise.all(this._evalPromises);
    return res;
  }

  /**
   * Put a file on the local (emulated) filesystem.
   */
  public putFile(filename: string, content: ArrayBuffer) {
    throw new NotImplementedError("putFile");
  }

  /**
   * Put an importable module on the local (emulated) filesystem
   * and load dependencies.
   */
  public putModule(filename: string, content: ArrayBuffer) {
    throw new NotImplementedError("putModule");
  }

  /**
   * Complete the submited code.
   */
  public async complete(code: string): Promise<[string[], number] | []> {
    return [];
  }

  /**
   * Tell wether we should wait for more code or if it can
   * be run as is.
   *
   * Useful to set ps1/ps2 in console prompt.
   */
  public async more(code: string): Promise<boolean> {
    throw new NotImplementedError("more");
  }

  /**
   * Send a message to the main thread to download a file.
   */
  public download(content: Uint8Array | string, filename: string): void {
    this.postMessage?.({
      type: "download",
      content: { content, filename },
    });
  }

  /**
   * Use JSON stringify/parse to copy an object.
   */
  protected clone(data: any) {
    return JSON.parse(JSON.stringify(data));
  }

  /**
   * Sync sleep for a certain duration in seconds.
   */
  public sleep(duration: number): void {
    duration = duration * 1000; // to milliseconds
    if (this.syncCommSupport()) {
      // ensure value before waiting
      Atomics.store(new Int32Array(this._sab!), 0, 0);
      Atomics.wait(new Int32Array(this._sab!), 0, 0, duration);
      return;
    } else {
      // default to active sleep
      const t0 = Date.now();
      while (Date.now() - t0 < duration);
    }
  }

  /**
   * Decode the string contained in a SharedArrayBuffer to
   * bypas this issue:
   * https://github.com/whatwg/encoding/issues/172
   */
  private decodeSharedArrayBuffer(sab: SharedArrayBuffer): string {
    const array = new Uint8Array(sab);
    let length = array.length;
    for (; length > 0 && array[length - 1] === 0; length--);
    if (length === 0) return "";
    const cropped = new Uint8Array(length);
    cropped.set(array.subarray(0, length));
    try {
      return new TextDecoder().decode(cropped);
    } catch (e) {
      if (array.length === length)
        throw new Error("String too long, can't decode");
      throw e;
    }
  }

  /**
   * Sync input that does not use the ugly prompt but send
   * an eval.input event to the main thread and wait for the response.
   */
  public input(
    prompt: string | null | undefined,
    password: boolean = false,
  ): string {
    const sendInput = () => {
      const data = this.clone(this.__eval_data__);
      this.postMessage?.({
        type: "input",
        content: { data, prompt, password },
      });
    };
    if (this.syncCommSupport()) {
      sendInput();
      // ensure value before waiting
      Atomics.store(new Int32Array(this._sab!), 0, 0);
      Atomics.wait(new Int32Array(this._sab!), 0, 0);
      return this.decodeSharedArrayBuffer(this._sab!);
    } else if (self._prompt != null) {
      // in legacy context, we default to js' prompt
      return self._prompt(prompt ?? "") ?? "";
    } else {
      sendInput();
      const timeout = 2 * 60 * 1000; // 2 minutes
      const tend = Date.now() + timeout;
      // polling loop
      while (Date.now() < tend) {
        const request = new XMLHttpRequest();
        request.open("POST", "https://api.basthon.fr/ephemeral", false); // synchronous request
        request.setRequestHeader("X-API-Referer", this._basthonRoot ?? "null");
        request.send(JSON.stringify({ action: "get", key: this._fallbackKey }));
        if (request.status === 200) return this._decrypt(request.responseText);
      }
      throw new Error("Error using the Basthon's network API");
    }
  }

  /**
   * Process messages from the main thread.
   */
  protected async processMainThreadMessage(msg: {
    data: any;
    type: string;
    content: any;
  }): Promise<void> {
    switch (msg.type) {
      case "toplevel-input":
        this._toplevelInputPromise?.resolve(msg.content as string);
        break;
      default: // this should not append...
        break;
    }
  }

  /**
   * Ask for an input in the main thread and wait for the answer
   * then return it.
   */
  public async toplevelInput(
    prompt: string | null | undefined,
    password: boolean = false,
    data: any = undefined,
  ): Promise<string> {
    this._toplevelInputPromise = new PromiseDelegate<string>();
    this.postMessage?.({
      type: "input",
      content: { data, prompt, password, toplevel: true },
    });
    const res = await this._toplevelInputPromise.promise;
    this._toplevelInputPromise = undefined;
    return res;
  }

  /**
   * Ask main thread to go to the next breakpoint.
   */
  public breakpointMoveOn(): void {
    const data = this.clone(this.__eval_data__);
    this.postMessage?.({ type: "breakpoint-move-on", content: { data } });
  }

  /**
   * Create the message channel between kernel and iframe,
   * send one port and the iframe to main thread and return the other one.
   */
  public displayKernelIFrame(iframeHTML: string): MessagePort {
    const { port1, port2 } = new MessageChannel();
    const content = { html: iframeHTML, port: port1 };
    const display_type = "kernel-iframe";
    const data = { ...this.clone(this.__eval_data__), content, display_type };
    this.postMessage?.({ data, type: "display" }, [port1]);
    return port2;
  }
}

export { ExecuteResult, KernelWorkerBase };
