import { transfer } from "comlink";

declare let self: DedicatedWorkerGlobalScope;

/**
 * A class for a proxy that allows a worker with opaque origin
 * to fetch assets.
 */
export class ProxyFetch {
  public async fetch(
    request: string | URL | Request,
    init?: RequestInit,
  ): Promise<{ body: any; options: any | undefined }> {
    request = new Request(request, init);
    const url = request.url;
    const parentURL = (s: string): string => s.substring(0, s.lastIndexOf("/"));
    const assetsURL = parentURL(new URL(globalThis.location.toString()).href);
    const baseURL = parentURL(new URL(url).href);
    if (!baseURL.startsWith(assetsURL))
      throw new Error(`URL ${url} is outside of proxy' scope`);
    // force credentials omition
    if (request.credentials !== "omit")
      throw new Error("Request tries to use credentials");
    const response = await globalThis.fetch(request);
    const buffer = await response.arrayBuffer();
    return {
      body: transfer(buffer, [buffer]),
      options: {
        status: response.status,
        statusText: response.statusText,
        // wrong types for headers in worker...
        // see https://github.com/microsoft/TypeScript/pull/15050/files#diff-1fe269c5fa7bcd3a57e9e6a91ce3566d3e3a5c38b21e6ff0d78ad0dd090f8d3aR24
        // @ts-ignore
        headers: new Map<string, string>(response.headers), // Headers are not clonable
      },
    };
  }
}
