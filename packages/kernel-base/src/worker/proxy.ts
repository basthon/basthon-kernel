import { transfer } from "comlink";

/**
 * A class for a proxy that allows a worker with opaque origin
 * to fetch assets.
 */
export class ProxyFetch {
  public async fetch(
    request: string | Request | URL,
    init: RequestInit | undefined
  ): Promise<{ body: any; options: any | undefined }> {
    const parentURL = (s: string): string => s.substring(0, s.lastIndexOf("/"));
    const assetsURL = parentURL(new URL(self.location.toString()).href);
    const url = (request as Request).url ?? request.toString();
    const baseURL = parentURL(new URL(url).href);
    if (baseURL !== assetsURL)
      throw new Error(`URL ${url} is outside of proxy' scope`);
    // force credentials omition
    if (init?.credentials !== "omit")
      throw new Error("Request tries to use credentials");
    const response = await self.fetch(request, init);
    const buffer = await response.arrayBuffer();
    return {
      body: transfer(buffer, [buffer]),
      options: {
        status: response.status,
        statusText: response.statusText,
        headers: new Map(response.headers), // Headers are not clonable
      },
    };
  }
}
