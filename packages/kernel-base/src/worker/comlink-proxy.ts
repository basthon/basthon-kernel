import { expose } from "comlink";
import { ProxyFetch } from "./proxy";

declare let self: DedicatedWorkerGlobalScope;

const proxy = new ProxyFetch();
expose(proxy);
