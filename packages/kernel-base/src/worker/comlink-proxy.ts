import { expose } from "comlink";
import { ProxyFetch } from "./proxy";

const proxy = new ProxyFetch();
expose(proxy);
