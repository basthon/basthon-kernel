import { PromiseDelegate } from "promise-delegate";
import { VERSION } from "./version";
import { NotImplementedError } from "./commons";

/**
 * Event for Basthon's dispatch/listen.
 */
class BasthonEvent extends Event {
  public detail: any;
  constructor(id: string, data: any) {
    super(id);
    this.detail = data;
  }
}

export { VERSION };

/**
 * API that any Basthon kernel should fill to be supported
 * in console/notebook.
 */
export class KernelBase {
  private _rootPath: string;
  private _isReady: boolean = false;
  private _ready = new PromiseDelegate<void>();
  private _stopped = Promise.resolve();
  protected _execution_count: number = 0;
  protected _pendingInput?: PromiseDelegate<string> = undefined;
  // a map to register wrapped listeners to allow later remove
  private _listeners = new Map<
    string,
    Map<EventListenerOrEventListenerObject, EventListenerOrEventListenerObject>
  >();
  protected _options: any;
  protected _files = new Map<string, ArrayBuffer>();
  protected _modules = new Map<string, ArrayBuffer>();

  constructor(options: any) {
    this._options = this.clone(options);
    // root path where kernel is installed
    this._rootPath = options.rootPath;
  }

  /**
   * Is this kernel safe? (loaded from an isolated web worker)
   */
  public safeKernel(): boolean {
    return false;
  }

  /**
   * Kernel version number (string).
   */
  public version(): string {
    return VERSION;
  }

  /**
   * Language implemented in the kernel (string).
   * Generally lower case.
   */
  public language(): string {
    throw new NotImplementedError("language");
  }

  /**
   * Language name implemented in the kernel (string).
   * As it should be displayed in text.
   */
  public languageName(): string {
    throw new NotImplementedError("languageName");
  }

  /**
   * Script (module) file extensions
   */
  public moduleExts(): string[] {
    throw new NotImplementedError("moduleExts");
  }

  /**
   * Execution count getter.
   */
  public get execution_count(): number {
    return this._execution_count;
  }

  /**
   * Async code evaluation that resolves with the result.
   */
  public evalAsync(
    code: string,
    outCallback: (_: string) => void,
    errCallback: (_: string) => void,
    data: any = null,
  ): Promise<any> {
    throw new NotImplementedError("evalAsync");
  }

  public async putFile(filename: string, content: ArrayBuffer): Promise<void> {
    this._files.set(filename, content);
  }

  public async putModule(
    filename: string,
    content: ArrayBuffer,
  ): Promise<void> {
    this._modules.set(filename, content);
  }

  public async userModules(): Promise<string[]> {
    return [];
  }

  /**
   * Get a file content from the VFS.
   */
  public async getFile(path: string): Promise<Uint8Array> {
    throw new NotImplementedError("getFile");
  }

  /**
   * Get a user module file content.
   */
  public async getUserModuleFile(filename: string): Promise<Uint8Array> {
    throw new NotImplementedError("getUserModuleFile");
  }

  public async more(source: string): Promise<boolean> {
    throw new NotImplementedError("more");
  }

  public async complete(code: string): Promise<[string[], number] | []> {
    return [];
  }

  public banner(): string {
    return `Welcome to the ${this.languageName()} REPL!`;
  }

  public ps1(): string {
    return ">>> ";
  }

  public ps2(): string {
    return "... ";
  }

  /**
   * Launch the kernel (used to wrap a promise around init).
   */
  protected async _init(): Promise<void> {}

  /**
   * Initialize the kernel and start it.
   */
  public async init(): Promise<void> {
    if (!this.safeKernel())
      console.warn(
        `%c⚠ DANGER ⚠: this kernel is not safe!` +
          " Please consider using a safe kernel.",
        "color: red; font-size: 20px;",
      );

    try {
      await this._init();
    } catch (error) {
      this._ready.reject(error);
      throw error;
    }
    // connecting eval to basthon.eval.request event.
    this.addEventListener("eval.request", this.evalFromEvent.bind(this));
    await this.start();
  }

  /**
   * Start the kernel (used to wrap a promise around start).
   */
  protected async _start(): Promise<void> {}

  /**
   * Start the kernel.
   */
  public async start(): Promise<void> {
    await this._stopped;
    this._execution_count = 0;

    try {
      await this._start();
    } catch (error) {
      this._ready.reject(error);
      throw error;
    }
    this._files.forEach((v, k) => this.putFile(k, v));
    this._modules.forEach((v, k) => this.putModule(k, v));
    this._isReady = true;
    this._ready.resolve();
  }

  /**
   * Perform async part of the stoping process.
   */
  protected async _stop(): Promise<void> {}

  /**
   * Stop the kernel synchronously (to ensure kernel is stopped after the call).
   */
  public stop(): void {
    this._isReady = false;
    this._ready = new PromiseDelegate<void>();
    this._stopped = this._stop();
  }

  /**
   * Restart the kernel (stop and start).
   * Use await this.ready() to ensure kernel is started
   */
  public restart(): void {
    this.stop(); // sync
    this.start(); // async
  }

  /**
   * Is the kernel ready?
   */
  public get isReady(): boolean {
    return this._isReady;
  }

  /**
   * Promise that resolves when the kernel is ready (started).
   */
  public async ready(): Promise<void> {
    return this._ready.promise;
  }

  /**
   * Root path for assets files.
   */
  public assetsURL(absolute: boolean = false): string {
    let url = this._rootPath;
    if (absolute && !url.startsWith("http")) {
      const base = window.location.origin + window.location.pathname;
      url = base.substring(0, base.lastIndexOf("/")) + "/" + url;
    }
    return url;
  }

  /**
   * Root for kernel files. This is always the language directory
   * inside the version number directory inside the assets directory.
   */
  public basthonRoot(absolute: boolean = false): string {
    return (
      this.assetsURL(absolute) + "/" + this.version() + "/" + this.language()
    );
  }

  /**
   * Downloading data (bytes array or data URL) as filename
   * (opening browser dialog).
   */
  public download(data: Uint8Array | string, filename: string): void {
    if (!(typeof data === "string" || data instanceof String)) {
      const blob = new Blob([data], { type: "application/octet-stream" });
      data = window.URL.createObjectURL(blob);
    }
    const anchor = document.createElement("a");
    anchor.download = filename;
    anchor.href = data as string;
    anchor.target = "_blank";
    anchor.style.display = "none"; // just to be safe!
    document.body.appendChild(anchor);
    anchor.click();
    document.body.removeChild(anchor);
  }

  /**
   * Wrapper around document.dispatchEvent.
   * It adds the 'basthon.' prefix to each event name and
   * manage the event lookup to retreive relevent data.
   */
  public dispatchEvent(eventName: string, data: any): void {
    document.dispatchEvent(new BasthonEvent(`basthon.${eventName}`, data));
  }

  /**
   * Wrapper around document.addEventListener.
   * It manages the 'basthon.' prefix to each event name and
   * manage the event lookup to retreive relevent data.
   */
  public addEventListener(eventName: string, callback: (_: any) => void): void {
    // wrapped callback
    const _callback = (event: Event) =>
      callback((event as BasthonEvent).detail);
    document.addEventListener(
      `basthon.${eventName}` as keyof DocumentEventMap,
      _callback,
    );
    // register the wrapped callback in order to remove it
    let listeners = this._listeners.get(eventName);
    if (listeners == null) {
      listeners = new Map<
        EventListenerOrEventListenerObject,
        EventListenerOrEventListenerObject
      >();
      this._listeners.set(eventName, listeners);
    }
    listeners.set(callback, _callback);
  }

  /**
   * Wrapper around document.removeEventListener.
   * It manages the 'basthon.' prefix to each event name.
   */
  public removeEventListener(
    eventName: string,
    callback: (_: any) => void,
  ): void {
    // get the wrapped callback
    const listeners = this._listeners.get(eventName);
    document.removeEventListener(
      `basthon.${eventName}` as keyof DocumentEventMap,
      listeners?.get(callback) as EventListenerOrEventListenerObject,
    );
  }

  /**
   * Send eval.input event then wait for the user response and return it.
   */
  public async inputAsync(
    prompt: string | null | undefined,
    password: boolean = false,
    data: any = undefined,
  ) {
    data = this.clone(data);
    data.content = { prompt, password };
    const pd = new PromiseDelegate<string>();
    data.resolve = pd.resolve.bind(pd);
    data.reject = pd.reject.bind(pd);
    this._pendingInput = pd;
    this.dispatchEvent("eval.input", data);
    const res = await pd.promise;
    this._pendingInput = undefined;
    return res;
  }

  /**
   * Close a pending input.
   */
  public resolvePendingInput(): void {
    this._pendingInput?.resolve("");
  }

  /**
   * Simple clone via JSON copy.
   */
  public clone(obj: any): any {
    // simple trick that is enough for our purpose.
    return JSON.parse(JSON.stringify(obj));
  }

  /**
   * Put a ressource (file or module).
   * Detection is based on extension.
   */
  public async putRessource(filename: string, content: ArrayBuffer) {
    const ext = filename.split(".").pop() ?? "";
    if (this.moduleExts().includes(ext)) {
      return await this.putModule(filename, content);
    } else {
      return await this.putFile(filename, content);
    }
  }

  /**
   * Is an input pending?
   */
  public pendingInput(): boolean {
    return this._pendingInput != null;
  }

  /**
   * Internal. Code evaluation after an eval.request event.
   */
  public async evalFromEvent(data: any) {
    const stdCallback = (std: string) => (text: string) => {
      let dataEvent = this.clone(data);
      dataEvent.stream = std;
      dataEvent.content = text;
      this.dispatchEvent("eval.output", dataEvent);
    };
    const outCallback = stdCallback("stdout");
    const errCallback = stdCallback("stderr");

    let args;
    try {
      args = await this.evalAsync(data.code, outCallback, errCallback, data);
    } catch (error: any) {
      errCallback(error.toString());
      const dataEvent = this.clone(data);
      dataEvent.error = error;
      dataEvent.execution_count = this.execution_count;
      this.dispatchEvent("eval.error", dataEvent);
      return;
    }
    if (args == null) return; // this should not happend
    const result = args[0];
    const executionCount = args[1];
    let dataEvent = this.clone(data);
    dataEvent.execution_count = executionCount;
    if (result != null) dataEvent.result = result;
    this.dispatchEvent("eval.finished", dataEvent);
  }
}
