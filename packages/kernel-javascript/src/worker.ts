import objectInspect from "object-inspect";
import { KernelWorkerBase } from "@basthon/kernel-base/worker";

export class JavaScriptKernelWorker extends KernelWorkerBase {
  constructor() {
    // do not forget to call the parent constructor
    super();
  }

  /*
   * Initialize the kernel.
   */
  protected async _init(): Promise<void> {
    self.console.info = (...args) => self.console.log(...args);
    self.console.warn = (...args) => self.console.error(...args);
    self.onerror = (message, source, lineno, colno, error) =>
      self.console.error(message);
    // the window object is not available in a webworker...
    self.eval("var window = self;");
  }

  protected _eval(data: any, code: string): any {
    self.console.log = (...args) => {
      this.sendStdoutStream(data, args.join(" ") + "\n");
    };

    self.console.error = (...args) => {
      this.sendStderrStream(data, args.join(" ") + "\n");
    };

    const result = self.eval(code);
    return typeof result === "undefined"
      ? undefined
      : { "text/plain": objectInspect(result) };
  }

  public complete(code: string): [string[], number] | [] {
    // basic completion based on global object
    const vars = Object.getOwnPropertyNames(self);
    const words = code.match(/(\w+)$/) ?? [];
    const word = words[0] ?? "";
    const matches = vars.filter((v) => v.startsWith(word));
    return [matches, code.length - word.length];
  }
}
