import objectInspect from "object-inspect";
import { KernelWorkerBase } from "@basthon/kernel-base/worker";

declare let self: DedicatedWorkerGlobalScope;

export class JavaScriptKernelWorker extends KernelWorkerBase {
  constructor(options?: any) {
    // do not forget to call the parent constructor
    super(options);
  }

  public language() {
    return "javascript";
  }

  /*
   * Initialize the kernel.
   */
  protected async _init(): Promise<void> {
    if (!this.isLegacy()) {
      console.info = (...args: any[]): void => console.log(...args);
      console.warn = (...args: any[]): void => console.error(...args);
      globalThis.addEventListener("error", (e: Event): void =>
        console.error(e.toString()),
      );
    }
  }

  protected async _eval(data: any, code: string): Promise<any> {
    console.log = (...args: any[]): void => {
      this.sendStdoutStream(data, args.join(" ") + "\n");
    };

    console.error = (...args: any[]): void => {
      this.sendStderrStream(data, args.join(" ") + "\n");
    };

    const result = await globalThis.eval(code); // if a promise is returned, wait for it
    return typeof result === "undefined"
      ? undefined
      : { "text/plain": objectInspect(result) };
  }

  /**
   * Special case of starting a legacy kernel.
   */
  public async legacyStart(): Promise<void> {}

  /**
   * Special case of stoping a legacy kernel.
   */
  public async legacyStop(): Promise<void> {}

  public putFile(filename: string, content: ArrayBuffer) {
    console.error(
      `File ${filename} not added since putFile has no mean in the JS context.`,
    );
  }

  public async putModule(filename: string, content: ArrayBuffer) {
    content = new Uint8Array(content);
    const ext = filename.split(".").pop();
    switch (ext) {
      case "js":
        const decoder = new TextDecoder("utf-8");
        const _content = decoder.decode(content);
        await globalThis.eval(_content);
        break;
      default:
        throw { message: "Only '.js' files supported." };
    }
  }

  public async complete(code: string): Promise<[string[], number] | []> {
    // basic completion based on global object
    const vars = Object.getOwnPropertyNames(self);
    const words = code.match(/(\w+)$/) ?? [];
    const word = words[0] ?? "";
    const matches = vars.filter((v) => v.startsWith(word));
    return [matches, code.length - word.length];
  }

  public async more(code: string): Promise<boolean> {
    return false;
  }
}
