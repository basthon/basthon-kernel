import { KernelMainBase } from "@basthon/kernel-base/worker";
import { JavaScriptKernelWorker } from "./worker";

export class JavaScriptKernel extends KernelMainBase<JavaScriptKernelWorker> {
  constructor(options: any) {
    super(options);
  }

  protected newWorker(): Worker {
    return new Worker(new URL("./comlink-worker.js", import.meta.url), {
      //@ts-ignore
      sandboxed: true, // removing this line will cause security issues
    });
  }

  protected async importLegacyWorker(): Promise<void> {
    await import("./comlink-worker.js");
  }

  public language() {
    return "javascript";
  }

  public languageName() {
    return "Javascript";
  }

  public moduleExts() {
    return ["js"];
  }

  public ps1() {
    return " js> ";
  }

  public ps2() {
    return "...> ";
  }
}
