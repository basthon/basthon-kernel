import { expose } from "@basthon/kernel-base/worker";
import { JavaScriptKernelWorker } from "./worker";

declare let self: DedicatedWorkerGlobalScope;

expose(JavaScriptKernelWorker);
