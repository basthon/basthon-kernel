import { expose } from "comlink";
import { JavaScriptKernelWorker } from "./worker";

const worker = new JavaScriptKernelWorker();

expose(worker);
