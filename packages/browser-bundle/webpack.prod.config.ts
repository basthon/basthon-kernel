import { Configuration } from "webpack";
import { merge } from "webpack-merge";
import base from "./webpack.base.config";
import path from "path";
import ForkTsCheckerWebpackPlugin from "fork-ts-checker-webpack-plugin";
import CopyPlugin from "copy-webpack-plugin";
import TerserPlugin from "terser-webpack-plugin";
import pkg from "./package.json";
import lerna from "../../lerna.json";

const kernelVersion = pkg.version === "0.0.0" ? lerna.version : pkg.version;

const config: Configuration = merge(base, {
  mode: "production",
  devtool: "source-map",
  module: {
    rules: [
      {
        test: /\.(tsx?|jsx?)$/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env", "@babel/preset-typescript"],
            plugins: ["@babel/plugin-transform-runtime"],
          },
        },
        // running babel on js_of_ocaml output breaks it
        exclude: /(node_modules|__kernel__\.js)/,
      },
    ],
  },
  plugins: [
    new ForkTsCheckerWebpackPlugin({
      typescript: {
        diagnosticOptions: {
          semantic: true,
          syntactic: true,
        },
      },
    }),
    new CopyPlugin({
      patterns: [
        {
          // python3 files
          from: "**/*",
          context: "../kernel-python3/lib/dist/",
          to: path.resolve(__dirname, "lib", kernelVersion, "python3"),
          toType: "dir",
        },
      ],
    }),
  ],
  optimization: {
    chunkIds: "named",
    minimize: true, // useless
    minimizer: [
      new TerserPlugin({
        // js_of_ocaml output is heavy and already minified
        exclude: /kernel-ocaml_lib___kernel___js.js/,
      }),
    ],
  },
});

export default config;
