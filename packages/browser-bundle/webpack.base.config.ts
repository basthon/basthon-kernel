import { Configuration } from "webpack";
import path from "path";

const config: Configuration = {
  entry: "./src/main.ts",
  output: {
    filename: "bundle.js",
    chunkFilename: "[name].js",
    path: path.resolve(__dirname, "lib"),
    clean: true,
  },
  module: {
    rules: [
      {
        // to ignore css import in kernel-loader
        test: /\.css$/i,
        use: "ignore-loader",
      },
      {
        test: /\.wasm$/i,
        type: "asset/inline",
      },
    ],
  },
  resolve: {
    extensions: [".ts", ".js"],
    fallback: {
      // for ocaml bundle
      constants: require.resolve("constants-browserify"),
      tty: require.resolve("tty-browserify"),
      vm: require.resolve("vm-browserify"),
      fs: false,
      child_process: false,
      // for sql bundle
      crypto: require.resolve("crypto-browserify"),
      path: require.resolve("path-browserify"),
      buffer: require.resolve("buffer/"),
      stream: require.resolve("stream-browserify"),
    },
  },
};

export default config;
