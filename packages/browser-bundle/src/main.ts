import { KernelLoader } from "@basthon/kernel-loader";

declare global {
  interface Window {
    basthonRoot?: string;
    basthonKernel?: string;
    basthonKernelLoader?: KernelLoader;
    basthonLegacy?: boolean;
  }
}

const loader = (window.basthonKernelLoader = new KernelLoader({
  rootPath: window.basthonRoot ?? "./",
  language: window.basthonKernel ?? "python3",
  legacy: window.basthonLegacy === true,
}));

(async () => {
  loader.showLoader("Chargement de Basthon...", true);
  await loader.kernelStarted();
})();
