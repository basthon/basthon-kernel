def test_put_file(selenium_py3):
    selenium_py3.run_js(
        """
    window.toBytesArray = function(string) {
        string = unescape(encodeURIComponent(string));
        const arr = [];
        for (var i = 0; i < string.length; i++) {
            arr.push(string.charCodeAt(i));
        }
        return Uint8ClampedArray.from(arr);
    }"""
    )

    content = "hello\n world! ¥£€$¢₡₢₣₤₥₦₧₨₩₪₫₭₮₯₹"
    selenium_py3.driver.execute_async_script(
        """
    const done = arguments[arguments.length - 1];
    const content = toBytesArray(arguments[0]);
    Basthon.putFile('foo.txt', content).then(done);
        """,
        content,
    )
    data = selenium_py3.run_basthon(
        """
    with open('foo.txt') as f:
        print(f.read(), end='', flush=True)"""
    )
    assert data["stderr"] == ""
    assert data["stdout"] == content


def test_put_module(selenium_py3):
    assert selenium_py3.run_js("return await Basthon.userModules();") == []
    content = "foo = 42"
    selenium_py3.driver.execute_async_script(
        """
    const done = arguments[arguments.length - 1];
    const content = toBytesArray(arguments[0]);
    Basthon.putModule('bar.py', content).then(done);
        """,
        content,
    )
    data = selenium_py3.run_basthon("import bar ; bar.foo")
    assert data["stdout"] == ""
    assert data["stderr"] == ""
    assert data["result"]["result"]["text/plain"] == "42"
    assert selenium_py3.run_js("return await Basthon.userModules();") == ["bar.py"]

    # with a second module

    content = "import cv2 ; bar = 24"
    selenium_py3.driver.execute_async_script(
        """
    const done = arguments[arguments.length - 1];
    const content = toBytesArray(arguments[0]);
    Basthon.putModule('foo.py', content).then(done);
        """,
        content,
    )
    data = selenium_py3.run_basthon("import foo; foo.bar")
    assert data["stdout"] == ""
    assert data["stderr"] == ""
    result = data["result"]["result"]["text/plain"]
    assert result == "24"
    assert selenium_py3.run_js("return await Basthon.userModules();") == [
        "bar.py",
        "foo.py",
    ]

    data = selenium_py3.run_basthon("foo.cv2.__version__")
    assert data["stdout"] == ""
    assert data["stderr"] == ""
    result = data["result"]["result"]["text/plain"]
    assert result == "'4.7.0'"


def test_put_hidden_module(selenium_py3):
    content = "def baz():\n    return 42\n"
    selenium_py3.driver.execute_async_script(
        """
    const done = arguments[arguments.length - 1];
    const content = toBytesArray(arguments[0]);
    Basthon.putModule('_foobar.py', content).then(done);
        """,
        content,
    )
    data = selenium_py3.run_basthon("import _foobar; _foobar.baz()")
    assert data["stdout"] == ""
    assert data["stderr"] == ""
    assert data["result"]["result"]["text/plain"] == "42"
    assert selenium_py3.run_js("return await Basthon.userModules();") == [
        "bar.py",
        "foo.py",
        "_foobar.pyc",
    ]
    data = selenium_py3.run_basthon("import inspect; inspect.getsource(_foobar.baz)")
    assert "OSError: could not get source code" in data["stderr"]
    assert data["stdout"] == ""
