from pathlib import Path
from utils import same_content
import re


def test_micropip(selenium_py3):
    # at this point, folium should have loaded requests
    # so we check that this is our version
    result = selenium_py3.run_basthon(
        """
    import requests
    requests.__author__ == 'Romain Casati'"""
    )
    assert result["stdout"] == ""
    assert result["stderr"] == ""
    assert result["result"]["result"]["text/plain"] == "True"


def test_scipy(selenium_py3):
    result = selenium_py3.run_basthon(
        """
from scipy.integrate import solve_ivp
import numpy as np

def f(t, y, a):
    return [-a * y[1], a * y[0]]

y0 = [1, 0]
sol = solve_ivp(f, (0, np.pi), y0, args=(2,), atol=1e-13, rtol=1e-10)
y = sol.y
np.allclose(y[:, -1], y0)"""
    )
    assert result["stdout"] == ""
    assert result["stderr"] == ""
    assert result["result"]["result"]["text/plain"] == "True"


def test_lolviz(selenium_py3):
    result = selenium_py3.run_basthon(
        """
import lolviz

ma_liste = ['hi', 'mom', {3, 4}, {"parrt":"user"}]

o = lolviz.listviz(ma_liste)
print(o.source)
o
"""
    )
    assert result["stderr"] == ""
    gv = re.sub("node[0-9]+", "node", result["stdout"])
    assert same_content(selenium_py3, "python3_lolviz_source.gv", gv)
    svg = re.sub("node[0-9]+", "node", result["result"]["result"]["image/svg+xml"])
    assert same_content(selenium_py3, "python3_lolviz.svg", svg)


def test_binarytree(selenium_py3):
    # literal_eval is the reverse of repr
    from ast import literal_eval

    result = selenium_py3.run_basthon(
        """
from binarytree import Node

root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)

print(root)
root.graphviz().source
"""
    )
    assert result["stderr"] == ""
    assert (
        result["stdout"]
        == """
    __1
   /   \\
  2     3
 / \\
4   5

"""
    )
    gv = literal_eval(result["result"]["result"]["text/plain"])
    gv = re.sub("[0-9]+:l", ":l", gv)
    gv = re.sub("[0-9]+:r", ":r", gv)
    gv = re.sub("[0-9]+:v", ":v", gv)
    gv = re.sub(r"[0-9]+ -> [0-9]+ \[label=", " ->  [label=", gv)
    gv = re.sub(r"[0-9]+ \[label=", " [label=", gv)
    assert same_content(selenium_py3, "python3_binarytree.gv", gv)


def test_audioop(selenium_py3):
    result = selenium_py3.run_basthon("import wave")
    assert result["stdout"] == ""
    assert result["stderr"] == ""


def test_doctest(selenium_py3):
    # this ensure our __main__ is properly connected to globals()
    # since doctest.testmod will look in __main__
    selenium_py3.run_basthon(
        """
import doctest

def my_test(x):
    '''
    >>> my_test(2)
    4
    >>> my_test(4)
    15
    '''
    return x ** 2
"""
    )
    data = selenium_py3.run_basthon("str(doctest.testmod(verbose=False))")
    assert data["stderr"] == ""
    assert (
        data["result"]["result"]["text/plain"] == "'TestResults(failed=1, attempted=2)'"
    )


def test_completion(selenium_py3):
    assert selenium_py3.run_js("return Basthon.complete('print.__d')") == [
        ["print.__delattr__(", "print.__dir__()", "print.__doc__"],
        0,
    ]


def test_validation_variables(selenium_py3):
    data = selenium_py3.run_basthon(
        """
from basthon.autoeval import ValidateVariables

test_variables = ValidateVariables({"a": 0.3, "b": False, "c": "hello world"})
"""
    )
    assert data["stderr"] == ""
    assert data["stdout"] == ""
    assert "result" not in data["result"]

    # undefined variables
    data = selenium_py3.run_basthon("test_variables()")
    assert (
        data["stderr"]
        == "La variable 'a' n'est pas définie, as-tu validé la cellule ?\n"
    )
    assert data["stdout"] == ""
    assert "result" not in data["result"]

    # all variables well defined
    data = selenium_py3.run_basthon(
        """
a = 0.1 + 0.2
b = a == 0.3
c = "hello world"

test_variables()
"""
    )
    assert data["stderr"] == ""
    assert data["stdout"] == "👏 Bravo, tes variables sont bien définies !\n"
    assert "result" not in data["result"]

    # check trial count
    data = selenium_py3.run_basthon("test_variables.trial_count()")
    assert data["stderr"] == ""
    assert data["stdout"] == ""
    assert data["result"]["result"]["text/plain"] == "2"


def test_validation_function(selenium_py3):
    data = selenium_py3.run_basthon(
        """
from basthon.autoeval import ValidateFunction

test_absolute_value = ValidateFunction(
    "absolute_value", range(-20, 21), valid_function=abs
)
"""
    )
    assert data["stderr"] == ""
    assert data["stdout"] == ""
    assert "result" not in data["result"]

    # undefined function
    data = selenium_py3.run_basthon("test_absolute_value()")
    assert (
        data["stderr"]
        == "La fonction 'absolute_value' n'est pas définie, as-tu validé la cellule ?\n"
    )
    assert data["stdout"] == ""
    assert "result" not in data["result"]

    # wrong function
    data = selenium_py3.run_basthon(
        """
absolute_value = float
test_absolute_value()
"""
    )
    assert (
        data["stderr"]
        == "Vérifie ta fonction 'absolute_value', au moins un test n'est pas passé...\n"
    )
    assert data["stdout"] == ""
    assert "result" not in data["result"]

    # function raising an error
    data = selenium_py3.run_basthon(
        """
def absolute_value(x):
    return 1 / 0
test_absolute_value()
"""
    )
    assert (
        re.sub("<basthon-input-[0-9]-[0-9a-f]*>", "<input>", data["stderr"])
        == """\
Traceback (most recent call last):
  File "<input>", line 3, in absolute_value
    return 1 / 0
           ~~^~~
ZeroDivisionError: division by zero
"""
    )
    assert data["stdout"] == ""
    assert "result" not in data["result"]

    # good function
    data = selenium_py3.run_basthon(
        """
def absolute_value(x):
    if x >= 0:
        return x
    else:
        return -x

test_absolute_value()
"""
    )
    assert data["stderr"] == ""
    assert (
        data["stdout"]
        == "👏 Bravo, ta fonction 'absolute_value' passe tous les tests !\n"
    )
    assert "result" not in data["result"]

    # check trial count
    data = selenium_py3.run_basthon("test_absolute_value.trial_count()")
    assert data["stderr"] == ""
    assert data["stdout"] == ""
    assert data["result"]["result"]["text/plain"] == "4"


def test_validation_function_pretty(selenium_py3):
    data = selenium_py3.run_basthon(
        """
from basthon.autoeval import ValidateFunctionPretty
from itertools import product

test_xor = ValidateFunctionPretty(
    "xor",
    product([True, False], repeat=2),
    valid_function=lambda x, y: x != y,
)
"""
    )
    assert data["stderr"] == ""
    assert data["stdout"] == ""
    assert "result" not in data["result"]

    # good function with pretty output
    data = selenium_py3.run_basthon(
        """
def xor(x, y):
    return x or y
test_xor()
"""
    )
    assert (
        data["stderr"]
        == "Vérifie ta fonction 'xor', au moins un test n'est pas passé...\n"
    )
    assert data["stdout"] == ""
    assert (
        data["display"]["content"]["text/html"]
        == '<table>\n                     <thead>\n                       <tr>\n                         <th>Test</th>\n                         <th>Attendu</th>\n                         <th>Calculé</th>\n                         <th></th>\n                       </tr>\n                     </thead>\n                     <tbody><tr class="bad">\n                        <td>xor(True, True)</td>\n                        <td>False</td>\n                        <td><b>True</b></td>\n                        <td>❌</td>\n                      </tr>\n<tr class="good">\n                        <td>xor(True, False)</td>\n                        <td>True</td>\n                        <td><b>True</b></td>\n                        <td>✅</td>\n                      </tr>\n<tr class="good">\n                        <td>xor(False, True)</td>\n                        <td>True</td>\n                        <td><b>True</b></td>\n                        <td>✅</td>\n                      </tr>\n<tr class="good">\n                        <td>xor(False, False)</td>\n                        <td>False</td>\n                        <td><b>False</b></td>\n                        <td>✅</td>\n                      </tr></tbody>\n                   </table>'
    )
    assert "result" not in data["result"]

    # check trial count
    data = selenium_py3.run_basthon("test_xor.trial_count()")
    assert data["stderr"] == ""
    assert data["stdout"] == ""
    assert data["result"]["result"]["text/plain"] == "1"


def test_validation_custom(selenium_py3):
    data = selenium_py3.run_basthon(
        """
from basthon.autoeval import validationclass
import math as _math
import __main__
import sys

@validationclass
class ValidateMathModule:
    def __call__(self):
        if hasattr(__main__, "math") and __main__.math == _math:
            print("👏 Bravo, tu as bien importé le module math !")
            return True
        else:
            print("Le module math n'est pas importé, attention à ne pas faire import * !",
                  file=sys.stderr)
            return False

test_math = ValidateMathModule()
"""
    )
    assert data["stderr"] == ""
    assert data["stdout"] == ""
    assert "result" not in data["result"]

    # undefined module
    data = selenium_py3.run_basthon("test_math()")
    assert (
        data["stderr"]
        == "Le module math n'est pas importé, attention à ne pas faire import * !\n"
    )
    assert data["stdout"] == ""
    assert "result" not in data["result"]

    # good import
    data = selenium_py3.run_basthon("import math ; test_math()")
    assert data["stderr"] == ""
    assert data["stdout"] == "👏 Bravo, tu as bien importé le module math !\n"
    assert "result" not in data["result"]

    # check trial count
    data = selenium_py3.run_basthon("test_math.trial_count()")
    assert data["stderr"] == ""
    assert data["stdout"] == ""
    assert data["result"]["result"]["text/plain"] == "2"


def test_validation_custom_2(selenium_py3):
    data = selenium_py3.run_basthon(
        """
from basthon.autoeval import ValidateVariables, validationclass

@validationclass
class ValidateHarshad(ValidateVariables):
    def __init__(self):
        super().__init__({"nombres_harshad": [1, 2, 3, 4, 5, 6, 7, 8, 9, 12]})

    def __call__(self):
        success = super().__call__()
        if not success and self.trial_count() >= 5:
            print("Je te donne un indice : un entier à un chiffre est toujours un nombre harshad.")
        return success

dix_premiers_harshad = ValidateHarshad()
"""
    )
    assert data["stderr"] == ""
    assert data["stdout"] == ""
    assert "result" not in data["result"]

    # nothing
    data = selenium_py3.run_basthon("for i in range(4): dix_premiers_harshad()")
    assert (
        data["stderr"]
        == "La variable 'nombres_harshad' n'est pas définie, as-tu validé la cellule ?\n"
        * 4
    )
    assert data["stdout"] == ""
    assert "result" not in data["result"]

    # hint
    data = selenium_py3.run_basthon("dix_premiers_harshad()")
    assert (
        data["stderr"]
        == "La variable 'nombres_harshad' n'est pas définie, as-tu validé la cellule ?\n"
    )
    assert (
        data["stdout"]
        == "Je te donne un indice : un entier à un chiffre est toujours un nombre harshad.\n"
    )
    assert "result" not in data["result"]

    # success
    data = selenium_py3.run_basthon(
        """
nombres_harshad = [1, 2, 3, 4, 5, 6, 7, 8, 9, 12]
dix_premiers_harshad()
"""
    )
    assert data["stderr"] == ""
    assert (
        data["stdout"] == "👏 Bravo, ta variable 'nombres_harshad' est bien définie !\n"
    )

    assert "result" not in data["result"]

    # check trial count
    data = selenium_py3.run_basthon("dix_premiers_harshad.trial_count()")
    assert data["stderr"] == ""
    assert data["stdout"] == ""
    assert data["result"]["result"]["text/plain"] == "6"


def test_validation_custom_bad(selenium_py3):
    data = selenium_py3.run_basthon(
        """
from basthon.autoeval import Validate

class MyValidate(Validate):
    pass

myvalidate = MyValidate()
"""
    )
    assert data["stderr"].endswith(
        "RuntimeError: You derived from Validate without using the @validationclass decorator\n"
    )
    assert data["stdout"] == ""
    assert "result" not in data["result"]

    data = selenium_py3.run_basthon(
        """
from basthon.autoeval import Validate

class MyValidate2(Validate):
    def __init__(self):
        super().__init__()

    def __call__(self):
        return super().__call__()

myvalidate = MyValidate2()
"""
    )
    assert data["stderr"].endswith(
        "RuntimeError: You derived from Validate without using the @validationclass decorator\n"
    )
    assert data["stdout"] == ""
    assert "result" not in data["result"]


def test_validation_function_signature(selenium_py3):
    data = selenium_py3.run_basthon(
        """
from basthon.autoeval import ValidateFunction

def myabs(x: float) -> float:
    return abs(x)

test_absolute_value = ValidateFunction(
    "absolute_value", range(-20, 21), valid_function=myabs,
    check_signature=True
)
"""
    )
    assert data["stderr"] == ""
    assert data["stdout"] == ""
    assert "result" not in data["result"]

    # wrong parameter name
    data = selenium_py3.run_basthon(
        """
def absolute_value(y: float) -> float:
    return abs(y)

test_absolute_value()
"""
    )
    assert (
        data["stderr"]
        == "Dans ta fonction 'absolute_value', le paramètre 'y' n'a pas le bon nom !\n"
    )
    assert data["stdout"] == ""
    assert "result" not in data["result"]

    # wrong number of parameters
    data = selenium_py3.run_basthon(
        """
def absolute_value(x: float, y: float) -> float:
    return abs(y)

test_absolute_value()
"""
    )
    assert (
        data["stderr"]
        == "Ta fonction 'absolute_value' n'a pas le bon nombre de paramètres !\n"
    )
    assert data["stdout"] == ""
    assert "result" not in data["result"]

    # wrong parameter type
    data = selenium_py3.run_basthon(
        """
def absolute_value(x: int) -> float:
    return abs(x)

test_absolute_value()
"""
    )
    assert (
        data["stderr"]
        == "Dans ta fonction 'absolute_value', le paramètre 'x' n'a pas le bon type !\n"
    )
    assert data["stdout"] == ""
    assert "result" not in data["result"]

    # wrong return type
    data = selenium_py3.run_basthon(
        """
def absolute_value(x: float) -> int:
    return abs(x)

test_absolute_value()
"""
    )
    assert (
        data["stderr"]
        == "Ta fonction 'absolute_value' n'a pas le bon type de retour !\n"
    )
    assert data["stdout"] == ""
    assert "result" not in data["result"]

    # good
    data = selenium_py3.run_basthon(
        """
def absolute_value(x: float) -> float:
    return abs(x)

test_absolute_value()
"""
    )
    assert data["stderr"] == ""
    assert (
        data["stdout"]
        == "👏 Bravo, ta fonction 'absolute_value' passe tous les tests !\n"
    )
    assert "result" not in data["result"]

    # without valid_function (only signature)
    data = selenium_py3.run_basthon(
        """
import inspect

valeurs = range(-20, 21)
test_absolute_value = ValidateFunction(
    "absolute_value", valeurs, target_values=[abs(x) for x in valeurs],
    check_signature=inspect.signature(myabs), ignore_names_in_signature=True
)
"""
    )
    assert data["stderr"] == ""
    assert data["stdout"] == ""
    assert "result" not in data["result"]

    # good
    data = selenium_py3.run_basthon(
        """
def absolute_value(thevalue: float) -> float:
    return abs(thevalue)

test_absolute_value()
"""
    )
    assert data["stderr"] == ""
    assert (
        data["stdout"]
        == "👏 Bravo, ta fonction 'absolute_value' passe tous les tests !\n"
    )
    assert "result" not in data["result"]


def test_easter_eggs(selenium_py3):
    data = selenium_py3.run_basthon("from basthon import __eastereggs__")
    assert data["stderr"] == ""
    assert data["stdout"] == ""
    assert "result" not in data["result"]

    data = selenium_py3.run_basthon("1 + 1")
    assert data["stderr"] == ""
    assert data["stdout"] == ""
    assert data["result"]["result"]["text/plain"] == "3"

    data = selenium_py3.run_basthon("'hello world'(print)")
    assert data["stderr"] == ""
    assert data["stdout"] == "hello world\n"
    assert "result" not in data["result"]

    data = selenium_py3.run_basthon("'hello world'(len)")
    assert data["stderr"] == ""
    assert data["stdout"] == ""
    assert data["result"]["result"]["text/plain"] == "11"
