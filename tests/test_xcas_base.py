from utils import same_content


def test_basics(selenium_xcas):
    data = selenium_xcas.run_basthon("solve(x^2 - 1)")
    assert data["stdout"] == ""
    assert data["stderr"] == ""
    result = data["result"]
    assert result["execution_count"] == 1
    assert result["result"]["text/latex"] == r"\[\{-1,1\}\]"

    data = selenium_xcas.run_basthon("1/3 + 1/6")
    assert data["stdout"] == ""
    assert data["stderr"] == ""
    result = data["result"]
    assert result["execution_count"] == 2
    result = result["result"]
    assert result["text/latex"] == r"\[\frac{1}{2}=0.5\]"

    data = selenium_xcas.run_basthon('"foo + bar = foobar"')
    assert data["stdout"] == ""
    assert data["stderr"] == ""
    result = data["result"]
    assert result["execution_count"] == 3
    result = result["result"]
    assert result["text/plain"] == 'text "foo + bar = foobar"'

    data = selenium_xcas.run_basthon('"ceci doit lever une erreur" ^ 2')
    assert data["stdout"] == "Error: Bad Argument Value\n"
    assert data["stderr"] == "Error: Bad Argument Value"
    result = data["result"]
    assert result["execution_count"] == 4
    assert "result" not in result

    data = selenium_xcas.run_basthon('print("foobar")')
    assert data["stdout"] == "foobar\n"
    assert data["stderr"] == ""
    result = data["result"]
    assert result["execution_count"] == 5
    result["result"]["text/plain"] = "0"

    data = selenium_xcas.run_basthon("def f(x): return x ** 2")
    assert data["stdout"] == "// Parsing f\n// Success\n// compiling f\n"
    assert data["stderr"] == ""
    result = data["result"]
    assert result["execution_count"] == 6
    result["result"]["text/plain"] = 'text "Done"'


def test_plot(selenium_xcas):
    data = selenium_xcas.run_basthon("plot(x)")
    assert data["stdout"] == ""
    assert data["stderr"] == ""
    result = data["result"]
    assert result["execution_count"] == 1
    result = result["result"]
    svg = result["image/svg+xml"]
    assert same_content(selenium_xcas, "xcas_plot_x.svg", svg)
