from pathlib import Path
import json
from utils import same_content
import re


def test_globals(selenium_py3):
    # should be placed after test_setup but pyodide.version() puts
    # `pyodide` in globale namespace
    data = selenium_py3.run_basthon("import __main__; sorted(dir(__main__))")
    assert data["stdout"] == ""
    assert data["stderr"] == ""
    assert data["result"]["result"]["text/plain"] == str(
        sorted(
            [
                "In",
                "Out",
                "_",
                "__",
                "___",
                "__builtins__",
                "__doc__",
                "__eval_data__",
                "__main__",
                "__name__",
            ]
        )
    )
    data = selenium_py3.run_basthon("a = 5")
    assert data["stdout"] == ""
    assert data["stderr"] == ""
    data = selenium_py3.run_basthon("a")
    assert data["stdout"] == ""
    assert data["stderr"] == ""
    assert data["result"]["result"]["text/plain"] == "5"


def test_setup(selenium_py3):
    data = selenium_py3.run_basthon("import pyodide; pyodide.__version__")
    assert data["result"]["result"]["text/plain"] == "'0.23.2'"
    data = selenium_py3.run_basthon("import sys ; sys.version.split(' ')[0]")
    assert data["result"]["result"]["text/plain"] == "'3.11.2'"
    # read lerna.json
    with open(Path(__file__).parents[1] / "lerna.json") as f:
        version = json.load(f)["version"]
    data = selenium_py3.run_basthon("import basthon ; basthon.__version__")
    assert data["result"]["result"]["text/plain"] == f"'{version}'"


def test_result(selenium_py3):
    for i in range(10):
        data = selenium_py3.run_basthon(f"{i} + {i}")
        assert data["stdout"] == ""
        assert data["stderr"] == ""
        assert data["result"]["result"]["text/plain"] == str(2 * i)
        assert data["result"]["execution_count"] == i + 1


def test_streams(selenium_py3):
    data = selenium_py3.run_basthon("print('foo')")
    assert data["stdout"] == "foo\n" and data["stderr"] == ""
    data = selenium_py3.run_basthon("import sys ; print('bar', file=sys.stderr)")
    assert data["stdout"] == "" and data["stderr"] == "bar\n"


def test_errors(selenium_py3):
    data = selenium_py3.run_basthon("1:")
    assert (
        re.sub("<basthon-input-[0-9]-[0-9a-f]*>", "<input>", data["stderr"])
        == '  File "<input>", line 1\n    1:\n     ^\nSyntaxError: invalid syntax\n'
    )
    assert "result" not in data["result"] and data["stdout"] == ""
    data = selenium_py3.run_basthon("1 / 0")
    assert (
        re.sub("<basthon-input-[0-9]-[0-9a-f]*>", "<input>", data["stderr"])
        == 'Traceback (most recent call last):\n  File "<input>", line 1, in <module>\n    1 / 0\n    ~~^~~\nZeroDivisionError: division by zero\n'
    )
    assert "result" not in data["result"] and data["stdout"] == ""


def test_flush(selenium_py3):
    # flushing should be performed even if not forced
    data = selenium_py3.run_basthon("print('foo', end='bar')")
    assert data["stdout"] == "foobar"


def test_importables(selenium_py3):
    result = selenium_py3.run_basthon(
        "from basthon import kernel ; kernel.importables()"
    )["result"]
    importables = eval(result["result"]["text/plain"])

    assert same_content(
        selenium_py3,
        "python3_importables.json",
        json.dumps(importables),
    )


def test_banner(selenium_py3):
    banner = selenium_py3.run_js("return Basthon.banner();")
    assert banner.startswith("Python 3.")
    assert (
        'Type "help", "copyright", "credits" or "license" for more information'
        in banner
    )
    banner_check = selenium_py3.run_js(
        "return await Basthon.remote.__kernel__.banner();"
    )
    assert banner == banner_check


def test_more(selenium_py3):
    tests = [
        ("for i in range(10)", False),
        ("for i in range(10):", True),
        (r"for i in range(10):\n    print(i)", True),
        (r"for i in range(10):\n    print(i)\n", False),
        ("def f(x)", False),
        ("def f(x):", True),
        (r"def f(x):\n    return 2 * x + 1", True),
        (r"def f(x):\n    return 2 * x + 1\n", False),
    ]
    for t, v in tests:
        assert selenium_py3.run_js(f"""return Basthon.more("{t}");""") == v
