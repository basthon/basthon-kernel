from pathlib import Path
import os


_test_data = Path(__file__).parent / "data"


def same_content(selenium, path: Path | str, data: str | bytes, binary=False):
    """
    Return True if data and the file content at `path` are equals.
    We carrefully backup data at the same destination but with
    '.generated_by_tests' suffix (before extension).
    """
    path = _test_data / path
    # read/write mode
    r, w = (f"{m}{'b' if binary else ''}" for m in "rw")

    # is content browser dependent?
    if not path.exists():
        browser = selenium.driver.capabilities["browserName"]
        path = path.parent / f"{path.stem}_{browser}{path.suffix}"

    # is content browser version dependent?
    paths = []
    if path.exists():
        paths.append(path)
    else:
        if "CI" in os.environ:
            job_name = os.environ["CI_JOB_NAME"]
            age = job_name.split("-")[-1]
            path = path.parent / f"{path.stem}_{age}{path.suffix}"
            paths.append(path)
        else:
            for age in ("old", "mid", "recent"):
                paths.append(path.parent / f"{path.stem}_{age}{path.suffix}")

    # backup the submitted content
    path_generated = path.parent / f"{path.stem}.generated_by_tests{path.suffix}"
    with open(path_generated, w) as f:
        f.write(data)

    # test against file
    for path in paths:
        with open(path, r) as f:
            if f.read() == data:
                return True
    return False
