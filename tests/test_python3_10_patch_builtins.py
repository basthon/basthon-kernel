from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions


def test_patch_input(selenium_py3):
    # toplevel input
    data = selenium_py3.run_basthon("input('How old are you?')")
    assert data["stderr"] == ""
    assert data["stdout"] == ""
    result = data["result"]["result"]["text/plain"]
    assert result == "'42'"

    # inner level input
    is_legacy = selenium_py3.run_js("return Basthon.isLegacy();")
    selenium_py3.run_basthon_detach(
        """
def f(x):
    return input(x)
f("How old are you?")
"""
    )

    if is_legacy:
        driver = selenium_py3.driver
        wait = WebDriverWait(driver, 10)
        wait.until(expected_conditions.alert_is_present())
        alert = driver.switch_to.alert
        alert.send_keys("42")
        alert.accept()

    data = selenium_py3.run_basthon_reattach()
    result = data["result"]["result"]["text/plain"]
    assert result == "'42'"
    assert data["stderr"] == ""
    assert data["stdout"] == ""
