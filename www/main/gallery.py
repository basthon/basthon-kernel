from pathlib import Path
import json
import urllib.parse


with (Path(__file__).parents[1] / "gallery.json").open() as f:
    examples = json.load(f)


item_template = """
    <div class="gallery-item">
      <div style="position: relative; top: 0; left: 0;">
        <a target="_blank" href="../images/galerie/{language}/{img_name}" style="position: relative; top: 0; left: 0; cursor: zoom-in;">
          <img src="../images/galerie/thumbnails/{language}/{img_name}" title="Voir l'image">
        </a>
        <a target="_blank" class="shadowed" href="{url}" style="position: absolute; top: calc(50% - 25px); left: calc(50% - 25px);">
          <img src="../images/play.svg" style="width: 50px;" title="Voir le code dans Basthon" class="python-yellow"/>
        </a>
      </div>
      <div class="desc">{description}</div>
    </div>"""


def url(file, language, aux, module, extensions):
    url = str(Path("examples") / file)
    domain = "notebook" if file.endswith(".ipynb") else "console"
    kernel = f"kernel={language}&" if language != "python3" else ""
    extensions = f"extensions={','.join(extensions)}&" if extensions else ""
    url = f"https://{domain}.basthon.fr/?{kernel}{extensions}from={urllib.parse.quote(url)}"
    if aux is not None:
        for a in [aux] if isinstance(aux, str) else aux:
            url += f"&aux={urllib.parse.quote(a)}"
    if module is not None:
        for m in [module] if isinstance(module, str) else module:
            url += f"&module={urllib.parse.quote(m)}"
    return url


def language_section(language: str) -> str:
    files = examples[language]
    kernel = language.lower().replace(" ", "")
    html = "".join(
        item_template.format(
            img_name=f"{Path(file).with_suffix('')}.png",
            description=data["desc"],
            url=url(
                file,
                kernel,
                data.get("aux"),
                data.get("module"),
                data.get("extensions"),
            ),
            language=language.lower().replace(" ", ""),
        )
        for file, data in files.items()
    )

    return f'<div class="gallery">{html}</div>'
