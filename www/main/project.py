import markdown
from pathlib import Path


def project() -> str:
    with (Path(__file__).parents[2] / "README.md").open() as f:
        return markdown.markdown(f.read(), extensions=["attr_list", "tables"])
