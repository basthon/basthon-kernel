from .gallery import language_section
from .project import project as _project


def define_env(env):
    """
    This is the hook for defining variables, macros and filters
    """

    @env.macro
    def gallery(language: str) -> str:
        return language_section(language)

    @env.macro
    def project() -> str:
        return _project()
