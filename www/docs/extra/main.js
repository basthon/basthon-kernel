// workaround to prevent downloading of external assets
const setupCICDBadge = () => {
  const cicdImg = document.querySelector(".ci-cd-badge > img");
  // not on main page?
  if (cicdImg == null) return;
  cicdImg.src =
    "https://forge.apps.education.fr/basthon/basthon-kernel/badges/master/pipeline.svg";
};

const ideLink = (ide) => {
  const url = new URL(window.location.href);
  if (url.hostname === "devel.basthon.fr")
    return `https://devel.basthon.fr/${ide}`;
  else return `https://${ide}.basthon.fr`;
};

/* kernel menus */
const setupBurgerMenu = () => {
  for (const ide of ["console", "notebook"]) {
    const parent = (() => {
      for (const e of document.querySelectorAll(
        "li.md-nav__item > label > span",
      ))
        if (e.innerText.toLowerCase().trim() === ide)
          return e.parentElement.parentElement;
    })();
    if (parent == null) continue;
    for (const elem of parent.querySelectorAll("nav > ul > li > a")) {
      const kernel = elem.innerText.toLowerCase().trim();
      elem.href = `${ideLink(ide)}/?kernel=${kernel}`;
      elem.setAttribute("target", "_blank");
      elem.setAttribute("rel", "noopener");
    }
  }
};

const setupNavBarMenu = () => {
  for (const ide of ["console", "notebook"]) {
    const parent = (() => {
      for (const e of document.querySelectorAll("a.md-tabs__link"))
        if (e.innerText.toLowerCase().trim() === ide) return e.parentElement;
    })();
    if (parent == null) continue;
    const link = parent.firstElementChild;
    link.href = ideLink(ide);
    link.setAttribute("target", "_blank");
    link.setAttribute("rel", "noopener");
    let child = parent.lastElementChild;
    if (child.tagName !== "UL") {
      child = document.createElement("ul");
      parent.appendChild(child);
      child.innerHTML = ["Python", "SQL", "OCaml", "JavaScript", "XCAS"]
        .map(
          (e) =>
            `<li><a href="${ideLink(ide)}/?kernel=${e.toLowerCase()}" target="_blank" rel="noopener">${e}</a></li>`,
        )
        .join("");
    }
    child.classList.add("header-dropdown-menu");
  }
};

const setup = () => {
  setupCICDBadge();
  setupBurgerMenu();
  setupNavBarMenu();
};

const observer = new MutationObserver((mutations) => {
  mutations.forEach((mutation) => {
    if (mutation.type === "childList") setup();
  });
});

observer.observe(document.body, { childList: true, subtree: true });
