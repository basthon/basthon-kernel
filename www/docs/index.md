---
icon: material/home
hide:
  - navigation
  - toc
---

# Basthon { .no-title }

[![ci/cd]()](https://forge.apps.education.fr/basthon/basthon-kernel/-/commits/master){ .ci-cd-badge }
![basthon logo](images/basthon.svg){ .basthon-logo-home }

<div id="subtitle">
  <div>Un bac à sable pour </div>
  <div class="language-scroller">
    <ul>
      <li>Python</li>
      <li>SQL</li>
      <li>Ocaml</li>
      <li>JS</li>
      <li>XCAS</li>
      <li>Python</li>
    </ul>
  </div>
  <div>, dans le navigateur !</div>
 </div>

[![Basthon-Console](images/basthon-console.svg)](https://console.basthon.fr "Basthon-Console"){ .basthon-logo-console-home }
[![Basthon-Notebook](images/basthon-notebook.svg)](https://notebook.basthon.fr "Basthon-Notebook"){ .basthon-logo-notebook-home }
