---
date:
  created: 2020-10-19 13:56:41+00:00
readtime: 1
authors:
  - romain
---

# Sympy pour le calcul formel

Aux collègues enseignants de mathématiques au lycée général, vous pouvez aussi utiliser [Basthon](https://basthon.fr) pour traiter le point "Exploiter un logiciel de calcul formel" grâce au module Sympy.

![](assets/twitted/1318189327676047360-EksdN87WMAAHVh0.jpg){ .md-center }

![](assets/twitted/1318189327676047360-EksdN9AXEAE6r11.jpg){ .md-center }

Même dans la version console, les formules s'affichent joliment avec la fonction `pretty_print`. Si vous préférez ne pas la présenter aux élèves, le `print` fonctionne également.

![](assets/twitted/1318189333841588225-EkseTgfWAAAiv-W.jpg){ .md-center }

Le recours à un logiciel de calcul formel apparaît dans les programmes de mathématiques de seconde, de premières technologique et générale et de terminale générale.

Je vous laisse le soin d'expliquer pourquoi cette écriture avec des crochets ne représente pas un intervalle !
