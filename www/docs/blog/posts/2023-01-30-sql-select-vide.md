---
date:
  created: 2023-01-30
readtime: 1
pin: true
categories:
  - SQL
tags:
  - nouveautés
authors:
  - romain
---

# `SELECT` vide en SQL

Du nouveau dans le noyau SQL de [Basthon](https://basthon.fr) : un `SELECT` vide affiche le nom des colonnes !

[Jouer avec !](https://notebook.basthon.fr/?kernel=sql&from=examples/sql-voitures.ipynb&module=examples/voitures.sql){ .md-button .md-center }

![Colonne SQL](assets/2023/select-vide-sql.jpg)
