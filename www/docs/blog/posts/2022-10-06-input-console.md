---
date:
  created: 2022-10-06
readtime: 1
pin: true
categories:
  - Python
tags:
  - nouveautés
authors:
  - romain
---

# Un vrai `input`

Et un vrai `input` pour la console et le notebook !

<video controls style="width: 50vw; height: 25vw;" >
  <source src="/assets/2022/input-console.mp4" type="video/mp4">
  Votre navigateur ne supporte pas la vidéo HTML5.
</video>

<video controls style="width: 50vw; height: 25vw;" >
  <source src="/assets/2022/input-notebook.mp4" type="video/mp4">
  Votre navigateur ne supporte pas la vidéo HTML5.
</video>
