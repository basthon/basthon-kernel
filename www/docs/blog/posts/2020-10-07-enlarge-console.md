---
date:
  created: 2020-10-07 21:23:02+00:00
readtime: 1
pin: true
categories:
  - Console
tags:
  - nouveautés
authors:
  - romain
---

# Disposition dans script/console

Le profil des utilisateurs de #Basthon se dessine peu à peu. Chacun choisi son camp. Il y a les adeptes des Notebooks et ceux qui préfèrent voir ce qui se passe dans la console lors de l'exécution de leur script.

Mais il y a une troisième catégorie de personnes à laquelle je n'avais pas pensé... ceux qui ne veulent que la console ! Pour eux et pour ceux qui se sentent à l'étroit, j'ai pensé à ceci.

![enlarge 1](assets/2020/enlarge-console-1.jpg)
![enlarge 2](assets/2020/enlarge-console-2.jpg)
![enlarge 3](assets/2020/enlarge-console-3.jpg)
