---
date:
  created: 2021-12-21 17:59:05+00:00
readtime: 1
authors:
  - romain
---

# Optimisation du démarrage

Le scénario de chargement de [Basthon](https://basthon.fr) a été entièrement repensé. De ~7s de chargement, on passe à ~1s pour rendre les interfaces disponibles (si les données sont déjà en cache).

<video controls style="width: 50vw; height: 25vw;">
  <source src="/assets/twitted/1473352337297133574-FHJl39mWQAcEe3Y.mp4" type="video/mp4">
</video>

En désactivant le cache, je mesure ~2s pour rendre l'interface disponible avec une connexion fibre et une machine standard.
