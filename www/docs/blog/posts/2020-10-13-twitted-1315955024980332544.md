---
date:
  created: 2020-10-13 09:58:21+00:00
readtime: 1
authors:
  - romain
---

# Ouverture d'un notebook vide

Vous m'avez sollicité pour que l'ouverture d'un notebook vierge soit supportée dans [Basthon](https://basthon.fr). C'est maintenant le cas !

![](assets/twitted/1315955024980332544-EkM2DghXcAIOoE-.png){ .md-center }
