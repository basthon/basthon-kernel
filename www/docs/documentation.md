---
hide:
  - navigation
  - toc
---

# Documentation

[:fontawesome-solid-file-pdf:](./pdf/Basthon_Documentation.pdf "Télécharger la documentation"){ .doc-download-pdf .md-center }

<iframe
  id="doc"
  src="../pdf/Basthon_Documentation.pdf"
  style="height: 100vw; width: 100%; border:none;"></iframe>
