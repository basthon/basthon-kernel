---
hide:
  - navigation
  - toc
---

# Galerie

Cliquer sur l'image pour l'agrandir et sur ![play](images/play.svg){ .play-icon-in-text } pour charger le code dans Basthon.

{% for lang in ["Python 3", "SQL", "OCaml", "JavaScript", "XCAS"] %}

=== "{{ lang }}"

    {{ gallery(lang) }}

{% endfor %}
