sphinx==7.2.6
sphinx-autodoc-typehints==1.25.2
sphinx-book-theme==1.1.0rc1
myst_parser==2.0.0
