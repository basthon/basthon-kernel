FROM casatir/basthon-kernel:test-amd64

ENV DOCKER_IMAGE="test-amd64-mid"

# Get Edge
RUN apt-get update
RUN apt-get install -y --no-install-recommends microsoft-edge-stable=106.0.1370.52-1
RUN rm -rf /var/lib/apt/lists/*
RUN wget -qO- https://msedgedriver.azureedge.net/$(microsoft-edge --version | cut -f3 -d' ')/edgedriver_linux64.zip | bsdtar -xvf- -C /opt/edge/
RUN chmod u+x /opt/edge/msedgedriver
ENV PATH="/opt/edge:${PATH}"

RUN echo 'PS1="\[\033[1;36m\](🐳 ${DOCKER_IMAGE})\[\033[0m\][\u@\h \W]\$ "' >> /etc/bash.bashrc
