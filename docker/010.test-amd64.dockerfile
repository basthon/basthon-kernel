FROM debian:bookworm-slim

ENV DOCKER_IMAGE="test-amd64"

# disable security and updates repos
RUN echo "deb http://deb.debian.org/debian bookworm main" > /etc/apt/sources.list
RUN apt-get update
RUN apt-get install -y --no-install-recommends sudo make nodejs npm chromium-driver firefox-esr \
  python3 python3-setuptools python3-pip python3-selenium \
  python3-pytest python3-pytest-xdist python3-pytest-instafail python3-pytest-rerunfailures \
  python3-pytest-httpserver python3-pytest-cov \
  wget bzip2 curl libarchive-tools libgconf-2-4 libxss1
RUN rm -rf /var/lib/apt/lists/*

# Get geckodriver
RUN wget -qO- https://github.com/mozilla/geckodriver/releases/download/v0.33.0/geckodriver-v0.33.0-linux64.tar.gz | tar zxC /usr/local/bin/

# Put Edge repository
RUN mkdir -p /opt/edge/
RUN apt-get update
RUN apt-get install -y --no-install-recommends gpg
RUN wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
RUN install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
RUN echo "deb [arch=amd64] https://packages.microsoft.com/repos/edge stable main" > /etc/apt/sources.list.d/microsoft-edge-dev.list
RUN rm microsoft.gpg


RUN echo 'PS1="\[\033[1;36m\](🐳 ${DOCKER_IMAGE})\[\033[0m\][\u@\h \W]\$ "' >> /etc/bash.bashrc
