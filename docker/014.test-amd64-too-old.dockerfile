FROM casatir/basthon-kernel:test-amd64

ENV DOCKER_IMAGE="test-amd64-too-old"

# We test against FF 56 since it is the first version to support headless selenium.
# Get firefox
RUN mkdir -p /opt/
RUN wget -qO- https://ftp.mozilla.org/pub/firefox/releases/56.0/linux-x86_64/fr/firefox-56.0.tar.bz2 | tar xjC /opt/
ENV PATH="/opt/firefox/:${PATH}"

# Get geckodriver (old driver for old browser)
RUN wget -qO- https://github.com/mozilla/geckodriver/releases/download/v0.19.0/geckodriver-v0.19.0-linux64.tar.gz | tar zxC /usr/local/bin/

# Roll back to older selenium version
RUN apt-get purge -y python3-selenium
RUN pip3 --no-cache-dir install --break-system-packages selenium==4.0.0a1

RUN echo 'PS1="\[\033[1;36m\](🐳 ${DOCKER_IMAGE})\[\033[0m\][\u@\h \W]\$ "' >> /etc/bash.bashrc
